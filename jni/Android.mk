LOCAL_PATH := $(call my-dir)

GLOBAL_LIB_TOP := $(LOCAL_PATH)
GLOBAL_PATH := $(LOCAL_PATH)

include $(CLEAR_VARS)

include $(GLOBAL_LIB_TOP)/glib-android/Android.mk
include $(GLOBAL_LIB_TOP)/fluidsynth-1.1.6/Android.mk
include $(GLOBAL_LIB_TOP)/soundengine/Android.mk


LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_CFLAGS := -w 
LOCAL_LDLIBS := -llog  
LOCAL_MODULE:= fluidsynth-1.1.6

FILE_SRC_LIST := $(wildcard $(LOCAL_PATH)/src/*.c)
FILE_SRC_LIST += $(wildcard $(LOCAL_PATH)/src/**/*.c)

LOCAL_SRC_FILES:= $(FILE_SRC_LIST:$(LOCAL_PATH)/%=%)

LOCAL_C_INCLUDES := $(LOCAL_PATH)/bindings/fluidmax/fluidsynth \
	$(LOCAL_PATH)/include \
	$(LOCAL_PATH)/include/external \
	$(LOCAL_PATH)/include/external/glib \
	$(LOCAL_PATH)/include/fluidsynth \
	$(LOCAL_PATH)/src \
	$(LOCAL_PATH)/src/midi \
	$(LOCAL_PATH)/src/rvoice \
	$(LOCAL_PATH)/src/sfloader \
	$(LOCAL_PATH)/src/synth \
	$(LOCAL_PATH)/src/utils \
	$(LOCAL_PATH)/src/drivers \
	$(LOCAL_PATH)/src/bindings
	
LOCAL_SHARED_LIBRARIES := glib-2.0 \
	gmodule-2.0 \
	gthread-2.0 \
	gobject-2.0 
	
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include
	
include $(BUILD_SHARED_LIBRARY) 
package pt.eartraining.intervals;

import junit.framework.Assert;
import android.content.Context;
import pt.eartraining.app.R;

public class IntervalUtils {	
	public static String arrayToString(Context context, int[] intervals) {
		return IntervalUtils.arrayToString(context, intervals, false);
	}
	
	public static String arrayToString(Context context, int[] intervals, boolean useLongName) {
		StringBuilder sb = new StringBuilder();
		
		if (intervals.length == 0) {
			return "No interval!";
		}
		
		for (int i = 0; i < intervals.length - 1; i++) {
			if (useLongName) {
//				sb.append(IntervalUtils.longName(context, intervals[i]) + ", ");
			} else {
//				sb.append(IntervalUtils.shortName(context, intervals[i]) + ", ");
			}
		}
		
		if (useLongName) {
//			sb.append(IntervalUtils.longName(context, intervals[intervals.length - 1]));
		} else {
//			sb.append(IntervalUtils.shortName(context, intervals[intervals.length - 1]));
		}
		
		return sb.toString();
	}
}

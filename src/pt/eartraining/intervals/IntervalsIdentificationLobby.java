package pt.eartraining.intervals;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.eartraining.app.R;
import pt.eartraining.core.DatabaseHelper.Column;
import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.QuizLobbyActivity;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.SpecificationDialogFragment;
import pt.eartraining.core.SpecificationsDataSource;
import pt.eartraining.musictheory.Interval;
import pt.eartraining.musictheory.IntervalType;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

public class IntervalsIdentificationLobby extends QuizLobbyActivity {
	@Override
	protected SpecificationDialogFragment getSpecDialogFragment() {
		return new SpecificationDialogFragment() {

			@Override
			protected QuizSpecification getSpecFromDialog(View dialogContentView) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected View getDialogContentView() {
				View view = LayoutInflater.from(getSherlockActivity()).inflate(R.layout.alert_dialog_custom_intervals, null);

				Spinner spinnerLowestNote = (Spinner) view.findViewById(R.id.spinner_lowest_note);
				Spinner spinnerHighestNote = (Spinner) view.findViewById(R.id.spinner_highest_note);
				spinnerLowestNote.setSelection(36 - 21);	// C2
				spinnerHighestNote.setSelection(96 - 21);	// C7
				spinnerLowestNote.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> adapter, View v,
							int pos, long id) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onNothingSelected(AdapterView<?> v) {
						// TODO Auto-generated method stub
					}
				});
				spinnerHighestNote.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> adapter, View v,
							int pos, long id) {
						// TODO Auto-generated method stub
					}

					@Override
					public void onNothingSelected(AdapterView<?> v) {
						// TODO Auto-generated method stub
					}
				});

				return view;
			}
		};
	}

	@Override
	protected SpecificationsDataSource getSpecsDataSource() {
		return new IntervalsIdentificationDataSource();
	}

	@Override
	protected Class<?> getQuizFactoryClass() {
		return IntervalsIdentificationQuizFactory.class;
	}

	@Override
	protected EarTrainingSubject getSubject() {
		return EarTrainingSubject.IntervalsIdentification;
	}

	public static class IntervalsIdentificationDataSource extends SpecificationsDataSource {
		private static final String COLUMN_INTERVALS = "intervals";
		private static final String COLUMN_INTERVAL_TYPES = "interval_types";
		private final String[] COLUMN_NAMES = { COLUMN_ID, COLUMN_SUBJECT, /* COLUMN_CUSTOM, */
				COLUMN_TITLE, COLUMN_SUBTITLE, COLUMN_VALIDATION_COUNT, COLUMN_INTERVALS,
				COLUMN_INTERVAL_TYPES };

		@Override
		public ArrayList<QuizSpecification> getSpecifications(SQLiteDatabase db, boolean custom) {
			ArrayList<QuizSpecification> specs = new ArrayList<QuizSpecification>();

			Cursor cursor = db.query(dataSourceName(), 
					COLUMN_NAMES,  COLUMN_CUSTOM + " = " + (custom ? 1 : 0), 
					null, null, null, COLUMN_ID);

			while (cursor.moveToNext()) {
				IntervalsSpecification spec = new IntervalsSpecification();
				spec.setId(cursor.getLong(0));
				spec.setSubject(EarTrainingSubject.fromInt(cursor.getInt(1)));
				spec.setTitle(cursor.getString(2));
				spec.setSubtitle(cursor.getString(3));
				spec.setValidationCount(cursor.getInt(4));
				spec.setIntervals(stringToIntervalsArray(cursor.getString(5)));
				spec.setIntervalTypes(stringToIntervalTypesArray(cursor.getString(6)));

				specs.add(spec);
			}

			return specs;
		}

		@Override
		public boolean addCustomSpec(SQLiteDatabase db, QuizSpecification spec) {

			return false;
		}

		@Override
		public void loadDefaultData(SQLiteDatabase db) {
			JSONArray array = SpecificationsDataSource.loadJSONFile(R.raw.intervals);

			try {
				db.beginTransaction();

				for (int i = 0; i < array.length(); i++) {
					JSONObject jsonSpec = array.getJSONObject(i);

					ContentValues values = new ContentValues();
					values.put(COLUMN_TITLE, jsonSpec.optString(COLUMN_TITLE, "Not Set"));
					values.put(COLUMN_SUBTITLE, jsonSpec.optString(COLUMN_SUBTITLE, "Not Set"));
					values.put(COLUMN_SUBJECT, getSubject().ordinal());
					values.put(COLUMN_CUSTOM, 0);
					values.put(COLUMN_VALIDATION_COUNT, jsonSpec.optInt(COLUMN_VALIDATION_COUNT, 10));
					values.put(COLUMN_INTERVALS, jsonSpec.getString(COLUMN_INTERVALS));
					values.put(COLUMN_INTERVAL_TYPES, jsonSpec.optString(COLUMN_INTERVAL_TYPES, IntervalType.MELODIC_ASCENDING.toString()));

					// Insert row into database
					long id = db.insert(dataSourceName(), null, values);

					if (id == -1) {
						Log.e("Error", "Could not insert '" + values.toString() + "'");
					} else {
						Log.i("DBInfo", "Inserted interval with id = " + id);
					}
				}

				db.setTransactionSuccessful();
			} catch (SQLException e) {

			} catch (JSONException e) {
				e.printStackTrace();
			} finally {
				db.endTransaction();
			}				
		}

		@Override
		public String dataSourceName() {
			return "intervals_identification";
		}

		@Override
		public Column[] dataSourceColumns() {
			Column[] columns = new Column[] {
					new Column(COLUMN_ID, Column.ColumnType.INTEGER, true),
					new Column(COLUMN_SUBJECT, Column.ColumnType.INTEGER, false),
					new Column(COLUMN_CUSTOM, Column.ColumnType.INTEGER, false),
					new Column(COLUMN_TITLE, Column.ColumnType.TEXT, false),
					new Column(COLUMN_SUBTITLE, Column.ColumnType.TEXT, false),
					new Column(COLUMN_VALIDATION_COUNT, Column.ColumnType.INTEGER, false),
					new Column(COLUMN_INTERVALS, Column.ColumnType.TEXT, false),
					new Column(COLUMN_INTERVAL_TYPES, Column.ColumnType.TEXT, false)
			};

			return columns;
		}

		@Override
		public EarTrainingSubject getSubject() {
			return EarTrainingSubject.IntervalsIdentification;
		}

		private Interval[] stringToIntervalsArray(String stringArray) {
			String[] elems = stringArray.split("[ ]*,[ ]*");
			Interval[] intervals = new Interval[elems.length];

			for (int i = 0; i < elems.length; i++) {
				intervals[i] = Interval.valueOf(elems[i]);
			}

			return intervals;
		}

		private IntervalType[] stringToIntervalTypesArray(String stringArray) {
			String[] elems = stringArray.split("[ ]*,[ ]*");
			IntervalType[] intervalTypes = new IntervalType[elems.length];

			for (int i = 0; i < elems.length; i++) {
				intervalTypes[i] = IntervalType.valueOf(elems[i]);
			}

			return intervalTypes;
		}
	}
}

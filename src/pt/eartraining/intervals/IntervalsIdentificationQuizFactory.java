package pt.eartraining.intervals;

import pt.eartraining.app.JBUtils;
import pt.eartraining.core.ButtonsQuiz;
import pt.eartraining.core.KeyboardQuiz;
import pt.eartraining.core.QuizFactory;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.musictheory.IntervalType;
import pt.eartraining.soundengine.MusicTrack;
import pt.eartraining.soundengine.MusicTrackFactory;
import android.os.Parcel;
import android.os.Parcelable;

public class IntervalsIdentificationQuizFactory extends QuizFactory {
	protected MusicTrackFactory mTrackFactory;

	public IntervalsIdentificationQuizFactory(QuizSpecification quizSpec) {
		super(quizSpec);

		mTrackFactory = new MusicTrackFactory();
	}

	@Override
	public ButtonsQuiz nextButtonQuiz() {
		IntervalsSpecification spec = (IntervalsSpecification) getQuizSpec();

		ButtonQuizAnswer[] answers = new ButtonQuizAnswer[spec.getIntervals().length];

		int root = getRandomRootNote(12);
		IntervalType type = JBUtils.randomElement(spec.getIntervalTypes());

		for (int i = 0; i < answers.length; i++) {
			MusicTrack track = mTrackFactory.newIntervalIdentificationTrack(
					root, spec.getIntervals()[i], type, true);

			answers[i] = new ButtonQuizAnswer(
					spec.getIntervals()[i].ordinal(), 
					spec.getIntervals()[i].getShortName(), 
					track);
		}

		ButtonQuizAnswer rightAnswer = answers[JBUtils.randInt(0, answers.length-1)];

		return new ButtonsQuiz(rightAnswer.getTrack(), answers, rightAnswer.getId());
	}

	@Override
	public KeyboardQuiz nextKeyboardQuiz() {
		// TODO Auto-generated method stub
		return null;
	}

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected IntervalsIdentificationQuizFactory(Parcel in) {
		super(in);
		
		mTrackFactory = new MusicTrackFactory();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	public static final Parcelable.Creator<IntervalsIdentificationQuizFactory> CREATOR = new Parcelable.Creator<IntervalsIdentificationQuizFactory>() {
		@Override
		public IntervalsIdentificationQuizFactory createFromParcel(Parcel in) {
			return new IntervalsIdentificationQuizFactory(in);
		}

		@Override
		public IntervalsIdentificationQuizFactory[] newArray(int size) {
			return new IntervalsIdentificationQuizFactory[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((mTrackFactory == null) ? 0 : mTrackFactory.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntervalsIdentificationQuizFactory other = (IntervalsIdentificationQuizFactory) obj;
		if (mTrackFactory == null) {
			if (other.mTrackFactory != null)
				return false;
		} else if (!mTrackFactory.equals(other.mTrackFactory))
			return false;
		return true;
	}
}
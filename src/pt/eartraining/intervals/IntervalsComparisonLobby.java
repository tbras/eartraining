package pt.eartraining.intervals;

import pt.eartraining.app.App;
import pt.eartraining.app.JBUtils;
import pt.eartraining.app.R;
import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.ButtonsQuiz;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.SpecificationsDataSource;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.musictheory.Interval;
import pt.eartraining.musictheory.IntervalType;
import pt.eartraining.soundengine.MusicTrack;
import android.content.Context;

public class IntervalsComparisonLobby extends IntervalsIdentificationLobby {
	@Override
	protected Class<?> getQuizFactoryClass() {
		return IntervalsComparisonQuizFactory.class;
	}

	@Override
	protected EarTrainingSubject getSubject() {
		return EarTrainingSubject.IntervalsComparison;
	}
	
	@Override
	protected SpecificationsDataSource getSpecsDataSource() {
		return new IntervalsComparisonDataSource();
	}
}


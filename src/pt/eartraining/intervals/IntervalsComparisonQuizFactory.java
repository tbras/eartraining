package pt.eartraining.intervals;

import java.util.Arrays;

import android.os.Parcel;
import android.os.Parcelable;

import pt.eartraining.app.App;
import pt.eartraining.app.JBUtils;
import pt.eartraining.app.R;
import pt.eartraining.core.ButtonsQuiz;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.musictheory.Interval;
import pt.eartraining.musictheory.IntervalType;
import pt.eartraining.soundengine.MusicTrack;

public class IntervalsComparisonQuizFactory extends IntervalsIdentificationQuizFactory {
	private static final int ANSWERS_COUNT = 2;

	private String[] mButtonsLabels;
	private int[] mIntervalsIndexes;

	public IntervalsComparisonQuizFactory(QuizSpecification quizSpec) {
		super(quizSpec);

		mButtonsLabels = new String[]{
				App.getContext().getString(R.string.first),
				App.getContext().getString(R.string.second)
		};

		int length = ((IntervalsSpecification) getQuizSpec()).getIntervals().length;

		assert(length >= 2);

		mIntervalsIndexes = new int[length];

		for (int i = 0; i < length; i++) {
			mIntervalsIndexes[i] = i;
		}
	}

	@Override
	public ButtonsQuiz nextButtonQuiz() {
		IntervalsSpecification spec = (IntervalsSpecification) getQuizSpec();

		// Shuffle array, and take the first two
		JBUtils.shuffleIntArray(mIntervalsIndexes);

		int root = getRandomRootNote(12);
		IntervalType type = JBUtils.randomElement(spec.getIntervalTypes());

		Interval first = spec.getIntervals()[mIntervalsIndexes[0]];
		Interval second = spec.getIntervals()[mIntervalsIndexes[1]];

		MusicTrack firstTrack = mTrackFactory.newIntervalIdentificationTrack(root, first, type, false);
		MusicTrack secondTrack = mTrackFactory.newIntervalIdentificationTrack(root, second, type, false);

		ButtonQuizAnswer[] answers = new ButtonQuizAnswer[ANSWERS_COUNT];
		answers[0] = new ButtonQuizAnswer(first.ordinal(), mButtonsLabels[0], firstTrack);
		answers[1] = new ButtonQuizAnswer(second.ordinal(), mButtonsLabels[1], secondTrack);

		// Both intervals are played, one after the other
		MusicTrack questionTrack = mTrackFactory.newIntervalComparisonTrack(root, first, type, second, type);

		// Right answer id is the biggest interval
		int rightAnswerId = first.getFormula()[1] > second.getFormula()[1] ? answers[0].getId() : answers[1].getId(); 

		return new ButtonsQuiz(questionTrack, answers, rightAnswerId);
	}

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected IntervalsComparisonQuizFactory(Parcel in) {
		super(in);
		mButtonsLabels = in.createStringArray();
		mIntervalsIndexes = in.createIntArray();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		
		dest.writeStringArray(mButtonsLabels);
		dest.writeIntArray(mIntervalsIndexes);
	}

	public static final Parcelable.Creator<IntervalsComparisonQuizFactory> CREATOR = new Parcelable.Creator<IntervalsComparisonQuizFactory>() {
		@Override
		public IntervalsComparisonQuizFactory createFromParcel(Parcel in) {
			return new IntervalsComparisonQuizFactory(in);
		}

		@Override
		public IntervalsComparisonQuizFactory[] newArray(int size) {
			return new IntervalsComparisonQuizFactory[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(mButtonsLabels);
		result = prime * result + Arrays.hashCode(mIntervalsIndexes);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntervalsComparisonQuizFactory other = (IntervalsComparisonQuizFactory) obj;
		if (!Arrays.equals(mButtonsLabels, other.mButtonsLabels))
			return false;
		if (!Arrays.equals(mIntervalsIndexes, other.mIntervalsIndexes))
			return false;
		return true;
	}
}

package pt.eartraining.intervals;

import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.intervals.IntervalsIdentificationLobby.IntervalsIdentificationDataSource;

public class IntervalsComparisonDataSource extends IntervalsIdentificationDataSource {
	@Override
	public String dataSourceName() {
		return "intervals_comparison";
	}
	
	@Override
	public EarTrainingSubject getSubject() {
		return EarTrainingSubject.IntervalsComparison;
	}
}

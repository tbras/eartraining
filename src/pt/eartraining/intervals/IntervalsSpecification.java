package pt.eartraining.intervals;

import java.util.Arrays;

import pt.eartraining.core.QuizSpecification;
import pt.eartraining.musictheory.Interval;
import pt.eartraining.musictheory.IntervalType;

import android.os.Parcel;
import android.os.Parcelable;

public class IntervalsSpecification extends QuizSpecification {
	private Interval[] mIntervals;
	private IntervalType[] mIntervalTypes;

	public IntervalsSpecification() {
		super();
	}

	@Override
	public String toString() {
		return "IntervalsSpecification [mIntervals="
				+ Arrays.toString(mIntervals) + ", mIntervalTypes="
				+ Arrays.toString(mIntervalTypes) + "]";
	}

	public Interval[] getIntervals() { return mIntervals; }
	public void setIntervals(Interval[] intervals) { mIntervals = intervals; }
	public IntervalType[] getIntervalTypes() { return mIntervalTypes; }
	public void setIntervalTypes(IntervalType[] intervalTypes) { mIntervalTypes = intervalTypes; }

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected IntervalsSpecification(Parcel in) {
		super(in);
		
		int[] intervals = in.createIntArray();
		mIntervals = new Interval[intervals.length];
		for (int i = 0; i < intervals.length; i++) {
			mIntervals[i] = Interval.fromInt(intervals[i]);
		}
		
		int[] intervalTypes = in.createIntArray();
		mIntervalTypes = new IntervalType[intervalTypes.length];
		for (int i = 0; i < intervalTypes.length; i++) {
			mIntervalTypes[i] = IntervalType.fromInt(intervalTypes[i]);
		}
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		
		int[] intervals = new int[mIntervals.length];
		for (int i = 0; i < intervals.length; i++) {
			intervals[i] = mIntervals[i].ordinal();
		}
		
		int[] intervalsTypes = new int[mIntervalTypes.length];
		for (int i = 0; i < intervalsTypes.length; i++) {
			intervalsTypes[i] = mIntervalTypes[i].ordinal();
		}
		
		dest.writeIntArray(intervals);
		dest.writeIntArray(intervalsTypes);
	}

	public static final Parcelable.Creator<IntervalsSpecification> CREATOR = new Parcelable.Creator<IntervalsSpecification>() {
		@Override
		public IntervalsSpecification createFromParcel(Parcel in) {
			return new IntervalsSpecification(in);
		}

		@Override
		public IntervalsSpecification[] newArray(int size) {
			return new IntervalsSpecification[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(mIntervalTypes);
		result = prime * result + Arrays.hashCode(mIntervals);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntervalsSpecification other = (IntervalsSpecification) obj;
		if (!Arrays.equals(mIntervalTypes, other.mIntervalTypes))
			return false;
		if (!Arrays.equals(mIntervals, other.mIntervals))
			return false;
		return true;
	}
}

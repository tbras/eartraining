package pt.eartraining.musictheory;

import pt.eartraining.app.App;
import pt.eartraining.app.R;
import android.util.SparseArray;

public enum IntervalType {
	MELODIC_ASCENDING(R.string.interval_type_melodic_ascending),
	MELODIC_DESCENDING(R.string.interval_type_melodic_descending),
	HARMONIC(R.string.interval_type_harmonic);
	
	private static final SparseArray<IntervalType> intToIntervalTypeMap = new SparseArray<IntervalType>();
	
	static {
		for (IntervalType i : IntervalType.values()) {
			intToIntervalTypeMap.put(i.ordinal(), i);
		}
	}
	
	public static IntervalType fromInt(int i) {
		return intToIntervalTypeMap.get(i);
	}
	
	private String mName;
	
	private IntervalType(int nameResId) {
		mName = App.getContext().getString(nameResId);
	}
	
	public String getName() { return mName; }
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

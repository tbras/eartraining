package pt.eartraining.musictheory;

import android.util.SparseArray;
import pt.eartraining.app.App;
import pt.eartraining.app.R;

public enum ChordProgression {
	I_V_I(R.string.progression_i_v_i, new int[][]{RomanNumeral.I, RomanNumeral.V_1_down, RomanNumeral.I}),
	Im_V_Im(R.string.progression_i_v_i, new int[][]{RomanNumeral.Im, RomanNumeral.V_1_down, RomanNumeral.Im}),
	I_V7_I(R.string.progression_i_v7_i, new int[][]{RomanNumeral.I, RomanNumeral.V7_1_down, RomanNumeral.I}),
	Im_V7_Im(R.string.progression_i_v_i, new int[][]{RomanNumeral.Im, RomanNumeral.V7_1_down, RomanNumeral.Im});
	
	
	private String mName;
	private int[][] mFormula;
	
	private static final SparseArray<ChordProgression> intToProgressionMap = new SparseArray<ChordProgression>();

	static {
		for (ChordProgression p : ChordProgression.values()) {
			intToProgressionMap.put(p.ordinal(), p);
		}
	}
	
	public static ChordProgression fromInt(int i) {
		return intToProgressionMap.get(i);
	}
	
	private ChordProgression(int nameResId, int[][] formula) {
		mName = App.getContext().getString(nameResId);
		mFormula = formula;
	}
	
	public String getName() { return mName; }
	public int[][] getFormula() { return mFormula; }
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
	
	public static class RomanNumeral {
		// Formulas
		public static final int[] I = {0, 12, 16, 19};
		public static final int[] Im = {0, 12, 15, 19};
		public static final int[] V = {7, 19, 23, 26};
		public static final int[] V_1_down = {7, 11, 14, 19};
		public static final int[] V7_1_down = {7, 11, 17, 19};
	}
}

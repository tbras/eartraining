package pt.eartraining.musictheory;

import android.util.SparseArray;
import pt.eartraining.app.App;
import pt.eartraining.app.R;

public enum Scale {
	MAJOR(R.string.scale_major_short, R.string.scale_major_long, new int[]{0, 2, 4, 5, 7, 9, 11}),
	MINOR_NATURAL(R.string.scale_minor_natural_short, R.string.scale_minor_natural_long, new int[]{0, 2, 3, 5, 7, 8, 10}),
	MINOR_MELODIC(R.string.scale_minor_melodic_short, R.string.scale_minor_melodic_long, new int[]{0, 2, 3, 5, 7, 9, 11}),
	MINOR_HARMONIC(R.string.scale_minor_harmonic_short, R.string.scale_minor_harmonic_long, new int[]{0, 2, 3, 5, 7, 8, 11}),
	MAJOR_PENTATONIC(R.string.scale_major_pentatonic_short, R.string.scale_major_pentatonic_long, new int[]{0, 2, 4, 7, 9}),
	MINOR_PENTATONIC(R.string.scale_minor_pentatonic_short, R.string.scale_minor_pentatonic_long, new int[]{0, 3, 5, 7, 10}),
	BLUES(R.string.scale_blues_short, R.string.scale_blues_long, new int[]{0, 3, 5, 6, 7, 10}),
	MODE_IONIAN(R.string.mode_ionian_short, R.string.mode_ionian_long, new int[]{0, 2, 4, 5, 7, 9, 11}),
	MODE_DORIAN(R.string.mode_dorian_short, R.string.mode_dorian_long, new int[]{0, 2, 3, 5, 7, 9, 10}),
	MODE_PHRYGIAN(R.string.mode_phrygian_short, R.string.mode_phrygian_long, new int[]{0, 1, 3, 5, 7, 8, 10}),
	MODE_LYDIAN(R.string.mode_lydian_short, R.string.mode_lydian_long, new int[]{0, 2, 4, 6, 7, 9, 11}),
	MODE_MIXOLYDIAN(R.string.mode_mixolydian_short, R.string.mode_mixolydian_long, new int[]{0, 2, 4, 5, 7, 9, 10}),
	MODE_AEOLIAN(R.string.mode_aeolian_short, R.string.mode_aeolian_long, new int[]{0, 2, 3, 5, 7, 8, 10}),
	MODE_LOCRIAN(R.string.mode_locrian_short, R.string.mode_locrian_long, new int[]{0, 1, 3, 5, 6, 8, 10});
	
	private static SparseArray<Scale> mIntToScaleMap = new SparseArray<Scale>();
	
	public static Scale fromInt(int i) {
		return mIntToScaleMap.get(i);
	}
	
	static {
		for (Scale s : Scale.values()) {
			mIntToScaleMap.put(s.ordinal(), s);
		}
	}
	
	private String mShortName;
	private String mLongName;
	private int[] mFormula;
	
	private Scale(int shortNameResId, int longNameResId, int[] formula) {
		mShortName = App.getContext().getString(shortNameResId);
		mLongName = App.getContext().getString(longNameResId);
		mFormula = formula;
	}
	
	public String getShortName() { return mShortName; }
	public String getLongName() { return mLongName; }
	public int[] getFormula() { return mFormula; }
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

package pt.eartraining.musictheory;

import pt.eartraining.app.JBUtils;

public enum Dynamics {
	DYNAMICS_FFF(126),
	DYNAMICS_FF(112),
	DYNAMICS_F(96),
	DYNAMICS_MF(80),
	DYNAMICS_MP(64),
	DYNAMICS_P(49),
	DYNAMICS_PP(33),
	DYNAMICS_PPP(16),
	DYNAMICS_RANDOM(0);
	
	private int mVel;
	
	private Dynamics(int vel) {
		mVel = vel;
	}
	
	public int getVel() {
		if (this != DYNAMICS_RANDOM) {
			return mVel;
		} else {
			return JBUtils.randInt(20, 110);
		}
	}
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

package pt.eartraining.musictheory;

import pt.eartraining.app.App;
import pt.eartraining.app.AppPreferences;
import pt.eartraining.app.R;
import android.util.SparseArray;

public enum Note {
	DO(R.string.note_do_romance, R.string.note_do_english),
	RE(R.string.note_re_romance, R.string.note_re_english),
	MI(R.string.note_mi_romance, R.string.note_mi_english),
	FA(R.string.note_fa_romance, R.string.note_fa_english),
	SOL(R.string.note_sol_romance, R.string.note_sol_english),
	LA(R.string.note_la_romance, R.string.note_la_english),
	SI(R.string.note_si_romance, R.string.note_si_english);
	
	private String mRomanceName;
	private String mEnglishName;

	private static final SparseArray<Note> intToIntervalMap = new SparseArray<Note>();

	static {
		for (Note n : Note.values()) {
			intToIntervalMap.put(n.ordinal(), n);
		}
	}
	
	public static Note fromInt(int i) {
		return intToIntervalMap.get(i);
	}

	private Note(int romanceNameResId, int englishNameResId) {
		mRomanceName = App.getContext().getString(romanceNameResId);
		mEnglishName = App.getContext().getString(englishNameResId);
	}
	
	public String getRomanceName() { return mRomanceName; }
	public String getEnglishName() { return mEnglishName; }
	public String getName(NoteNamingSystem system) {
		return (system == NoteNamingSystem.ROMANCE) ? getRomanceName() : getEnglishName();
	}
	public String getName() {
		if (AppPreferences.getInstance().getNoteNamingSystem() == NoteNamingSystem.ROMANCE) {
			return getRomanceName();
		} else {
			return getEnglishName();
		}
	}
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

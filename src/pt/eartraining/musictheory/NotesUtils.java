package pt.eartraining.musictheory;

import pt.eartraining.app.R;
import android.content.Context;
import android.util.SparseIntArray;

public class NotesUtils {
	public static final int MIDI_LOWEST_NOTE = 21;
	public static final int MIDI_HIGHEST_NOTE = 108;
	
	private static String[] NOTES_NAMES = {
		Note.DO.getName(),
		Note.DO.getName() + "#",
		Note.RE.getName(),
		Note.RE.getName() + "#",
		Note.MI.getName(),
		Note.FA.getName(),
		Note.FA.getName() + "#",
		Note.SOL.getName(),
		Note.SOL.getName() + "#",
		Note.LA.getName(),
		Note.LA.getName() + "#",
		Note.SI.getName(),
	};

	public static int getOctaveFirstNote(int octave) {
		return (octave + 1) * 12;
	}

	public static int getOctaveLastNote(int octave) {
		return getOctaveFirstNote(octave + 1);
	}

	public static int getOctave(int note) {
		return (note / 12) - 1;
	}

	// TODO: implement this
	//	public static String getNoteName(Context context, ScaleType scale, int note) {
	//		return String.valueOf(note);
	//	}

	public static String getNoteName(int note) {
		return getNoteName(note, true);
	}

	public static String getNoteName(int note, boolean octave) {
		if (octave) {
			return NOTES_NAMES[note % 12] + String.valueOf(getOctave(note));
		} else {
			return NOTES_NAMES[note % 12];
		}
	}

	public enum Notes {
		NOTE_C,
		NOTE_C_SHARP,
		NOTE_C_SHARP2,
		NOTE_C_FLAT,
		NOTE_C_FLAT2,
		NOTE_D,
		NOTE_D_SHARP,
		NOTE_D_SHARP2,
		NOTE_D_FLAT,
		NOTE_D_FLAT2,
		NOTE_E,
		NOTE_E_SHARP,
		NOTE_E_SHARP2,
		NOTE_E_FLAT,
		NOTE_E_FLAT2,
		NOTE_F,
		NOTE_F_SHARP,
		NOTE_F_SHARP2,
		NOTE_F_FLAT,
		NOTE_F_FLAT2,
		NOTE_G,
		NOTE_G_SHARP,
		NOTE_G_SHARP2,
		NOTE_G_FLAT,
		NOTE_G_FLAT2,
		NOTE_A,
		NOTE_A_SHARP,
		NOTE_A_SHARP2,
		NOTE_A_FLAT,
		NOTE_A_FLAT2,
		NOTE_B,
		NOTE_B_SHARP,
		NOTE_B_SHARP2,
		NOTE_B_FLAT,
		NOTE_B_FLAT2
	}
}

package pt.eartraining.musictheory;

import pt.eartraining.app.App;
import pt.eartraining.app.R;
import android.util.SparseArray;

/**
 * Interval defined as an int array of size 2, which the first element is ALWAYS zero
 * 
 * @author tiagobras
 *
 */
public enum Interval {
	UNISON(R.string.interval_unison_short, R.string.interval_unison_long, new int[]{0, 0}),
	MINOR_2ND(R.string.interval_minor_2nd_short, R.string.interval_minor_2nd_long, new int[]{0, 1}),
	MAJOR_2ND(R.string.interval_major_2nd_short, R.string.interval_major_2nd_long, new int[]{0, 2}),
	MINOR_3RD(R.string.interval_minor_3rd_short, R.string.interval_minor_3rd_long, new int[]{0, 3}),
	MAJOR_3RD(R.string.interval_major_3rd_short, R.string.interval_major_3rd_long, new int[]{0, 4}),
	PERFECT_4TH(R.string.interval_perfect_4th_short, R.string.interval_perfect_4th_long, new int[]{0, 5}),
	TRITONE(R.string.interval_tritone_short, R.string.interval_tritone_long, new int[]{0, 6}),
	PERFECT_5TH(R.string.interval_perfect_5th_short, R.string.interval_perfect_5th_long, new int[]{0, 7}),
	MINOR_6TH(R.string.interval_minor_6th_short, R.string.interval_minor_6th_long, new int[]{0, 8}),
	MAJOR_6TH(R.string.interval_major_6th_short, R.string.interval_major_6th_long, new int[]{0, 9}),
	MINOR_7TH(R.string.interval_minor_7th_short, R.string.interval_minor_7th_long, new int[]{0, 10}),
	MAJOR_7TH(R.string.interval_major_7th_short, R.string.interval_major_7th_long, new int[]{0, 11}),
	OCTAVE(R.string.interval_perfect_8th_short, R.string.interval_perfect_8th_long, new int[]{0, 12}),
	MINOR_9TH(R.string.interval_minor_9th_short, R.string.interval_minor_9th_long, new int[]{0, 13}),
	MAJOR_9TH(R.string.interval_major_9th_short, R.string.interval_major_9th_long, new int[]{0, 14}),
	MINOR_10TH(R.string.interval_minor_10th_short, R.string.interval_minor_10th_long, new int[]{0, 15}),
	MAJOR_10TH(R.string.interval_major_10th_short, R.string.interval_major_10th_long, new int[]{0, 16}),
	MINOR_11TH(R.string.interval_minor_11th_short, R.string.interval_minor_11th_long, new int[]{0, 17}),
	MAJOR_11TH(R.string.interval_major_11th_short, R.string.interval_major_11th_long, new int[]{0, 18});

	private String mShortName;
	private String mLongName;
	private int[] mFormula;

	private static final SparseArray<Interval> intToIntervalMap = new SparseArray<Interval>();

	static {
		for (Interval i : Interval.values()) {
			intToIntervalMap.put(i.ordinal(), i);
		}
	}
	
	public static Interval fromInt(int i) {
		return intToIntervalMap.get(i);
	}

	private Interval(int shortNameResId, int longNameResId, int[] formula) {
		mShortName = App.getContext().getString(shortNameResId);
		mLongName = App.getContext().getString(longNameResId);
		mFormula = formula;
	}
	
	public String getShortName() { return mShortName; }
	public String getLongName() { return mLongName; }
	public int[] getFormula() { return mFormula; }
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

package pt.eartraining.musictheory;

import android.util.SparseArray;
import pt.eartraining.app.App;
import pt.eartraining.app.R;

public enum Chord {
	MAJOR(R.string.chord_major_short, R.string.chord_major_long, new int[]{0, 4, 7}), 
	MINOR(R.string.chord_minor_short, R.string.chord_minor_long, new int[]{0, 3, 7}),
	SUS_4(R.string.chord_sus4_short, R.string.chord_sus4_long, new int[]{0, 5, 7}),
	SUS_2(R.string.chord_sus2_short, R.string.chord_sus2_long, new int[]{0, 2, 7}),
	AUG(R.string.chord_aug_short, R.string.chord_aug_long, new int[]{0, 4, 8}), 
	DIM(R.string.chord_dim_short, R.string.chord_dim_long, new int[]{0, 3, 6}), 
	MAJOR_7(R.string.chord_maj7_short, R.string.chord_maj7_long, new int[]{0, 4, 7, 11}), 
	DOM_7(R.string.chord_7_short, R.string.chord_7_long, new int[]{0, 4, 7, 10}), 
	MINOR_7(R.string.chord_m7_short, R.string.chord_m7_long, new int[]{0, 3, 7, 10}),
	MINOR_MAJOR_7(R.string.chord_mMaj7_short, R.string.chord_mMaj7_long, new int[]{0, 3, 7, 11}), 
	DIM_7(R.string.chord_dim7_short, R.string.chord_dim7_long, new int[]{0, 3, 6, 9}), 
	MINOR_7_B5(R.string.chord_m7b5_short, R.string.chord_m7b5_long, new int[]{0, 3, 6, 10}), 
	SUS_7(R.string.chord_7sus_short, R.string.chord_7sus_long, new int[]{0, 5, 7, 1});

	private String mShortName;
	private String mLongName;
	private int[] mFormula;

	private static final SparseArray<Chord> intToChordMap = new SparseArray<Chord>();

	static {
		for (Chord c : Chord.values()) {
			intToChordMap.put(c.ordinal(), c);
		}
	}
	
	public static Chord fromInt(int i) {
		return intToChordMap.get(i);
	}

	private Chord(int shortNameResId, int longNameResId, int[] formula) {
		mShortName = App.getContext().getString(shortNameResId);
		mLongName = App.getContext().getString(longNameResId);
		mFormula = formula;
	}
	
	public String getShortName() { return mShortName; }
	public String getLongName() { return mLongName; }
	public int[] getFormula() { return mFormula; }
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

package pt.eartraining.musictheory;

import pt.eartraining.app.App;
import pt.eartraining.app.R;
import android.util.SparseArray;

public enum NoteNamingSystem {
	ROMANCE(R.string.note_naming_system_romance), 
	ENGLISH(R.string.note_naming_system_english);
	
	private static final SparseArray<NoteNamingSystem> intToNoteNamingSystemMap = new SparseArray<NoteNamingSystem>();
	
	static {
		for (NoteNamingSystem n : NoteNamingSystem.values()) {
			intToNoteNamingSystemMap.put(n.ordinal(), n);
		}
	}
	
	public static NoteNamingSystem fromInt(int i) {
		return intToNoteNamingSystemMap.get(i);
	}
	
	private String mName;
	
	private NoteNamingSystem(int nameResId) {
		mName = App.getContext().getString(nameResId);
	}
	
	public String getName() { return mName; }
	
	public int[] ordinals() {
		int[] ordinals = new int[values().length];
		
		for (int i = 0; i < ordinals.length; i++) {
			ordinals[i] = values()[i].ordinal();
		}
		
		return ordinals;
	}
}

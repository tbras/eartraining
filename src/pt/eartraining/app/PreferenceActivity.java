package pt.eartraining.app;

import pt.eartraining.core.DatabaseHelper;
import pt.eartraining.soundengine.FluidSynth;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.Log;

import com.actionbarsherlock.app.SherlockPreferenceActivity;

public class PreferenceActivity extends SherlockPreferenceActivity {
	private static final int FILE_DIALOG_REQUEST = 1;
	
	private AppPreferences prefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);

		prefs = AppPreferences.getInstance();
		prefs.updateAllSummaries(this);
		prefs.getSharedPreferences().registerOnSharedPreferenceChangeListener(listener);

		// TODO: find a better way to do this
		Preference p = findPreference("key_clear_data");
		p.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				new AlertDialog.Builder(PreferenceActivity.this)
				.setTitle("Clear data")
				.setMessage("Are REALLY you sure? This action is NOT reversible.")
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						DatabaseHelper.clearAllData();
					}})
					.setNegativeButton(android.R.string.no, null).show();
				return true;
			}
		});
		
		Preference p1 = findPreference(getResources().getString(R.string.pref_key_soundfont_path));
		p1.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				showFileDialog();
			    
				return true;
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		Log.i("info", "Hello!!");
		
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == FILE_DIALOG_REQUEST) {
				String path = data.getDataString();
				
				if (".sf2".equalsIgnoreCase(path.substring(path.lastIndexOf(".")))) {
					prefs.setSoundFontPath(path.replaceFirst("file://", ""));
					
					// Only update preferences if sound path was able to load
					if (FluidSynth.getInstance().refreshSettings()) {
						prefs.updateSoundFontPathSummary(PreferenceActivity.this);
					}
				} else {
					new AlertDialog.Builder(PreferenceActivity.this)
					.setTitle(getString(R.string.alert_dialog_invalid_file))
					.setMessage(getString(R.string.alert_dialog_invalid_file_message))
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(android.R.string.yes, null).show();
				}
			}
		}
	}

	protected void onDestroy() {
		super.onDestroy();

		prefs.getSharedPreferences().unregisterOnSharedPreferenceChangeListener(listener);
	};


	OnSharedPreferenceChangeListener listener = new OnSharedPreferenceChangeListener() {
		@Override
		public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
				String key) {
			prefs.updateSummary(PreferenceActivity.this, key);
		}
	}; 
	
	protected void showFileDialog() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
	    intent.setType("file/*");
	    
	    startActivityForResult(intent, FILE_DIALOG_REQUEST);
	}
}

package pt.eartraining.app;

import java.io.File;
import java.util.Arrays;

import pt.eartraining.chords.ChordsIdentificationLobby;
import pt.eartraining.chords.ChordsProgressionsLobby;
import pt.eartraining.custom.MainScreenCardView;
import pt.eartraining.dictation.MelodicDictationLobby;
import pt.eartraining.intervals.IntervalsComparisonLobby;
import pt.eartraining.intervals.IntervalsIdentificationLobby;
import pt.eartraining.musictheory.Chord;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class ActivityMain extends SherlockActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Card Intervals Comparison
		MainScreenCardView card1 = (MainScreenCardView)findViewById(R.id.card_intervals_comparison);
		card1.setOnClickListener(cardsOnClickListener);

		MainScreenCardView card2 = (MainScreenCardView)findViewById(R.id.card_intervals_identification);
		card2.setOnClickListener(cardsOnClickListener);

		MainScreenCardView card3 = (MainScreenCardView)findViewById(R.id.card_chords_identification);
		card3.setOnClickListener(cardsOnClickListener);
		
		MainScreenCardView card4 = (MainScreenCardView)findViewById(R.id.card_chords_progressions);
		card4.setOnClickListener(cardsOnClickListener);
		
		MainScreenCardView card5 = (MainScreenCardView)findViewById(R.id.card_melodic_dictation);
		card5.setOnClickListener(cardsOnClickListener);

		AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

		Chord a = Chord.valueOf("MAJOR");
		Log.i("Test", a.getLongName() + Arrays.toString(a.getFormula()));

		Log.i("Info", "Max volume: " + audioManager.getStreamVolume(AudioManager.STREAM_MUSIC));

		int max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

		Log.i("Info", "Max volume: " + max);

		copySoundFontIfNeeded();
	}

	@Override
	protected void onResume() {
		super.onResume();

		Log.d("Info", "onResume()");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getSupportMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Log.i("Info", "FeatureId: " + featureId + ", Item: " + item);


		switch(item.getItemId()) {
		case R.id.preferences: 
			Intent intent = new Intent(this, PreferenceActivity.class);
			startActivity(intent);
			return true;
		default:return super.onMenuItemSelected(featureId, item);
		}
	}

	private OnClickListener cardsOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int id = v.getId();
			Intent intent = null;

			if (id == R.id.card_intervals_comparison) {
				intent = new Intent(ActivityMain.this, IntervalsComparisonLobby.class);
			} else if (id == R.id.card_intervals_identification) {
				intent = new Intent(ActivityMain.this, IntervalsIdentificationLobby.class);
			} else if (id == R.id.card_chords_identification) {
				intent = new Intent(ActivityMain.this, ChordsIdentificationLobby.class);
			} else if (id == R.id.card_chords_progressions) {
				intent = new Intent(ActivityMain.this, ChordsProgressionsLobby.class);
			} else if (id == R.id.card_melodic_dictation) {
				intent = new Intent(ActivityMain.this, MelodicDictationLobby.class);
			}

			if (intent != null) {
				startActivity(intent);
			}
		}
	};

	private void copySoundFontIfNeeded() {
		AppPreferences app = AppPreferences.getInstance();

		String sfPath = app.getSoundFontPath();

		File file = new File(sfPath);

		if (file.exists()) {
			Log.d("Info", "Soundfont do exist! Ready for loading.");
			return;
		}

		Log.i("Info", "Soundfont doesn't exist. Copying...");

		String absFilepath = JBUtils.copySoundfont(this, "instruments.mp3");

		if (absFilepath == null) {
			Log.e("Error", "Could not copy default soundfontPath. Check if there's enough space.");
			return;
		}

		app.setSoundFontPath(absFilepath);
	}

	//
	//	Handler mHandler = new Handler();
	//
	//	final Runnable r = new Runnable() {
	//		private void loadNote(RandomAccessFile file, int note, byte[] buffer) {
	//			try {
	//				file.seek(PCMAudio.NOTE_SAMPLES_SIZE_IN_BYTES * note);
	//			} catch (IOException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//			}
	//			try {
	//				file.read(buffer);
	//			} catch (IOException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//			}
	//		}
	//
	//		@Override
	//		public void run() {
	//			byte[] buffer = new byte[88200];
	//			final int maxBuffSize = PCMAudio.getAudioTrackMaxBufferSize();
	//
	//			final AudioTrack audioTrack = PCMAudio.createAudioTrack();
	//			audioTrack.play();
	//
	//			RandomAccessFile file = null;
	//			try {
	//				file = new RandomAccessFile(filepath, "r");
	//			} catch (FileNotFoundException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//			}
	//
	//			final int MAX_BLOCK_SIZE = maxBuffSize;	
	//
	//			for (int i = 0; i < 4; i++) {
	//				int note = JBUtils.randInt(50-21,  70-21);
	//
	//				loadNote(file, note, buffer);
	//
	//				int samplesLeft = 88200;
	//
	//				Log.d("Debug", "Playing...");
	//				while (samplesLeft > 0) {
	//					int blockSize = samplesLeft < MAX_BLOCK_SIZE ? samplesLeft : MAX_BLOCK_SIZE;
	//
	//					audioTrack.write(buffer, 88200 - samplesLeft, blockSize);
	//
	//					samplesLeft -= blockSize;
	//				}
	//				Log.d("Debug", "Stoppin...");
	//			}
	//
	//		}
	//	};
}

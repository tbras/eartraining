package pt.eartraining.app;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import pt.eartraining.musictheory.NoteNamingSystem;

import junit.framework.Assert;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.Preference;
import android.preference.PreferenceManager;

public class AppPreferences {
	// DON'T CHANGE THESE KEYS. Their values are used in strings.xml (Preferences Keys)
	private static final String KEY_SOUNDFONT_PATH = "SoundFontPath";
	private static final String KEY_SAMPLE_RATE = "SampleRate";
	private static final String KEY_STEREO = "Stereo";
	private static final String KEY_TEMPO_IN_BPM = "Tempo";
	private static final String KEY_NOTE_VELOCITY = "NoteVelocity";
	private static final String KEY_CACHE_SIZE = "CacheSize";
	private static final String KEY_PLAYBACK_GAIN = "PlaybackGain";
	private static final String KEY_LOWEST_NOTE = "LowestNote";
	private static final String KEY_HIGHEST_NOTE = "HighestNote";
	private static final String KEY_NOTE_NAMING_SYSTEM = "NoteNamingSystem";
	// TODO: implement these bellow and intervals music/song references
	//	private static final String KEY_PRE_CACHE_ALL_PIANO_NOTES = "PreCacheAllPianoNotes";
	//	private static final String KEY_PRE_LOAD_SOUNDS = "PreLoadSounds";

	private static final String OWN_VALUE_CARD = "{?}";

	private static final String APP_SHARED_PREFS = "AppPreferences"; 
	private SharedPreferences sharedPrefs;
	private Editor editor;

	private static AppPreferences mInstance = null;

	public static AppPreferences getInstance() {
		if (mInstance == null) {
			mInstance = new AppPreferences();
		}

		return mInstance;
	}

	private AppPreferences() {
		this.sharedPrefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
		this.editor = sharedPrefs.edit();
	}

	public SharedPreferences getSharedPreferences() {
		return sharedPrefs;
	}

	public NoteNamingSystem getNoteNamingSystem() {
		return NoteNamingSystem.valueOf(sharedPrefs.getString(KEY_NOTE_NAMING_SYSTEM, NoteNamingSystem.ENGLISH.toString()));
	}

	public boolean setNoteNamingSystem(NoteNamingSystem namingSystem) {
		editor.putString(KEY_NOTE_NAMING_SYSTEM, namingSystem.toString());
		editor.commit();
		return true;
	}

	public void updateNoteNamingSystemSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_NOTE_NAMING_SYSTEM);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_note_naming_system);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, getNoteNamingSystem().getName());
		}

		p.setSummary(summary);
	}

	public String getSoundFontPath() {
		return sharedPrefs.getString(KEY_SOUNDFONT_PATH, 
				App.getContext().getString(R.string.pref_def_soundfont_path));
	}

	public boolean setSoundFontPath(String soundFontPath) {
		editor.putString(KEY_SOUNDFONT_PATH, soundFontPath);
		editor.commit();
		return true;
	}

	public void updateSoundFontPathSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_SOUNDFONT_PATH);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_soundfont_path);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getSoundFontPath()));
		}

		p.setSummary(summary);
	}

	public int getSampleRate() {
		return Integer.parseInt(sharedPrefs.getString(KEY_SAMPLE_RATE, 
				App.getContext().getString(R.string.pref_def_sample_rate)));
	}

	public boolean setSampleRate(int sampleRate) {
		editor.putString(KEY_SAMPLE_RATE, String.valueOf(sampleRate));
		editor.commit();
		return true;
	}

	public void updateSampleRateSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_SAMPLE_RATE);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_sample_rate);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getSampleRate()));
		}

		p.setSummary(summary);
	}

	public boolean getStereo() {
		return sharedPrefs.getBoolean(KEY_STEREO, 
				Boolean.parseBoolean(App.getContext().getString(R.string.pref_def_stereo)));
	}

	public boolean setStereo(boolean stereo) {
		editor.putBoolean(KEY_STEREO, stereo);
		editor.commit();
		return true;
	}

	public void updateStereoSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_STEREO);

		if (p == null) {
			return;
		}

		String summary;

		if (getStereo()) {
			summary= App.getContext().getString(R.string.pref_summary_stereo_on);
		} else {
			summary = App.getContext().getString(R.string.pref_summary_stereo_off);
		}

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getStereo()));
		}

		p.setSummary(summary);
	}

	public int getTempoInBPM() {
		return Integer.parseInt(sharedPrefs.getString(KEY_TEMPO_IN_BPM, 
				App.getContext().getString(R.string.pref_def_tempo_in_bpm)));
	}

	public boolean setTempoInBPM(int tempoInBPM) {
		editor.putString(KEY_TEMPO_IN_BPM, String.valueOf(tempoInBPM));
		editor.commit();
		return true;
	}

	public void updateTempoInBPMSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_TEMPO_IN_BPM);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_tempo_in_bpm);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getTempoInBPM()));
		}

		p.setSummary(summary);
	}

	public int getNoteVelocity() {
		return sharedPrefs.getInt(KEY_NOTE_VELOCITY, 
				Integer.parseInt(App.getContext().getString(R.string.pref_def_note_velocity)));
	}

	public boolean setNoteVelocity(int noteVelocity) {
		editor.putInt(KEY_NOTE_VELOCITY, noteVelocity);
		editor.commit();
		return true;
	}

	public void updateNoteVelocitySummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_NOTE_VELOCITY);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_note_velocity);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getNoteVelocity()));
		}

		p.setSummary(summary);
	}

	public int getCacheSize() {
		return Integer.parseInt(sharedPrefs.getString(KEY_CACHE_SIZE, 
				App.getContext().getString(R.string.pref_def_cache_size)));
	}

	public boolean setCacheSize(int cacheSizeMiB) {
		editor.putString(KEY_NOTE_VELOCITY, String.valueOf(cacheSizeMiB));
		editor.commit();
		return true;
	}

	public void updateCacheSizeSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_CACHE_SIZE);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_cache_size);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getCacheSize()));
		}

		p.setSummary(summary);
	}

	public double getPlaybackGain() {
		return Double.parseDouble(sharedPrefs.getString(KEY_PLAYBACK_GAIN, 
				App.getContext().getString(R.string.pref_def_playback_gain)));
	}

	public boolean setPlaybackGain(double gain) {
		DecimalFormat format = new DecimalFormat("#.#", new DecimalFormatSymbols(Locale.US));

		editor.putString(KEY_PLAYBACK_GAIN, format.format(gain));
		editor.commit();
		return true;
	}

	public void updatePlaybackGainSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_PLAYBACK_GAIN);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_playback_gain);

		if (summary.contains(OWN_VALUE_CARD)) {
			DecimalFormat format = new DecimalFormat("#.#", new DecimalFormatSymbols(Locale.US));

			summary = summary.replace(OWN_VALUE_CARD, format.format(getPlaybackGain()));
		}

		p.setSummary(summary);
	}

	public int getLowestNote() {
		return Integer.parseInt(sharedPrefs.getString(KEY_LOWEST_NOTE, 
				App.getContext().getString(R.string.pref_def_lowest_note)));
	}

	public boolean setLowestNote(int lowestNote) {
		editor.putString(KEY_LOWEST_NOTE, String.valueOf(lowestNote));
		editor.commit();
		return true;
	}

	public void updateLowestNoteSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_LOWEST_NOTE);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_lowest_note);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getLowestNote()));
		}

		p.setSummary(summary);
	}

	public int getHighestNote() {
		return Integer.parseInt(sharedPrefs.getString(KEY_HIGHEST_NOTE, 
				App.getContext().getString(R.string.pref_def_highest_note)));
	}

	public boolean setHighestNote(int highestNote) {
		editor.putString(KEY_HIGHEST_NOTE, String.valueOf(highestNote));
		editor.commit();
		return true;
	}

	public void updateHighestNoteSummary(PreferenceActivity preferenceActivity) {
		Preference p = preferenceActivity.findPreference(KEY_HIGHEST_NOTE);

		if (p == null) {
			return;
		}

		String summary =  App.getContext().getString(R.string.pref_summary_highest_note);

		if (summary.contains(OWN_VALUE_CARD)) {
			summary = summary.replace(OWN_VALUE_CARD, String.valueOf(getHighestNote()));
		}

		p.setSummary(summary);
	}

	// Helpers
	public void updateSummary(PreferenceActivity preferenceActivity, String key) {		
		if (key.equals(KEY_SOUNDFONT_PATH)) {
			updateSoundFontPathSummary(preferenceActivity); 
		} else if (key.equals(KEY_SAMPLE_RATE)) {
			updateSampleRateSummary(preferenceActivity);
		} else if (key.equals(KEY_STEREO)) {
			updateStereoSummary(preferenceActivity);
		} else if (key.equals(KEY_TEMPO_IN_BPM)) {
			updateTempoInBPMSummary(preferenceActivity);
		} else if (key.equals(KEY_NOTE_VELOCITY)) {
			updateNoteVelocitySummary(preferenceActivity);
		} else if (key.equals(KEY_CACHE_SIZE)) {
			updateCacheSizeSummary(preferenceActivity);
		} else if (key.equals(KEY_PLAYBACK_GAIN)) {
			updatePlaybackGainSummary(preferenceActivity);
		} else if (key.equals(KEY_LOWEST_NOTE)) {
			updateLowestNoteSummary(preferenceActivity);
		} else if (key.equals(KEY_HIGHEST_NOTE)) {
			updateHighestNoteSummary(preferenceActivity);
		} else if (key.equals(KEY_NOTE_NAMING_SYSTEM)) {
			updateNoteNamingSystemSummary(preferenceActivity);
		} else {
			Assert.fail("Preference key not defined: " + key);
		}
	}

	public void updateAllSummaries(PreferenceActivity preferenceActivity) {
		updateSoundFontPathSummary(preferenceActivity); 
		updateSampleRateSummary(preferenceActivity);
		updateStereoSummary(preferenceActivity);
		updateTempoInBPMSummary(preferenceActivity);
		updateNoteVelocitySummary(preferenceActivity);
		updateCacheSizeSummary(preferenceActivity);
		updatePlaybackGainSummary(preferenceActivity);
		updateLowestNoteSummary(preferenceActivity);
		updateHighestNoteSummary(preferenceActivity);
		updateNoteNamingSystemSummary(preferenceActivity);
	}
}

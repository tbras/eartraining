package pt.eartraining.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class ActivitySeekBarDialog extends SherlockFragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if (savedInstanceState != null) {
			SherlockDialogFragment newFragment = new SeekBarDialogFragment();
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			
		}
	}
	
	void showDialog() {
        DialogFragment newFragment = SeekBarDialogFragment.newInstance(
                R.string.alert_dialog_seek_bar_title);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void doPositiveClick() {
        // Do stuff here.
        Log.i("FragmentAlertDialog", "Positive click!");
    }

    public void doNegativeClick() {
        // Do stuff here.
        Log.i("FragmentAlertDialog", "Negative click!");
    }
	
	public static class SeekBarDialogFragment extends SherlockDialogFragment {
		public static SeekBarDialogFragment newInstance(int title) {
			SeekBarDialogFragment frag = new SeekBarDialogFragment();
            Bundle args = new Bundle();
            args.putInt("title", title);
            frag.setArguments(args);
            return frag;
        }
		
		@Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage("Oh YEAH FIRE!")
	               .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // FIRE ZE MISSILES!
	                   }
	               })
	               .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog
	                   }
	               });
	 
	        return builder.create();
	    }
		
		@Override
		public void onDismiss(DialogInterface dialog) {
			super.onDismiss(dialog);
			
			getSherlockActivity().finish();
		}
	}
}

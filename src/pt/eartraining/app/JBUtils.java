package pt.eartraining.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;

import junit.framework.Assert;

/**
 * @author Tiago Bras
 *
 */
public class JBUtils {
	private static final Random rand = new Random();
	
	/**
	 * Returns a random element of @param array.
	 * 
	 * @param array	an int array
	 * @return a random element of <code>array</code>
	 */
	public static int randomElement(int []array) {
		return array[rand.nextInt(array.length)];
	}

	public static <T> T randomElement(T[] array) {
		return array[rand.nextInt(array.length)];
	}

	public static <T> T randomElement(ArrayList<T> arrayList) {
		return arrayList.get(rand.nextInt(arrayList.size()));
	}

	/**
	 * Gets the max number of an int array
	 * 
	 * @param array an int array
	 * @return the max number of <code>array</array>
	 */
	public static int max(int[] array) {
		int max = array[0];

		for (int i = 1; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}

		return max;
	}

	public static int randInt(int min, int max) {
		max = min > max ? min : max;

		if (min == max) {
			return min;
		}

		return min + rand.nextInt(max - min + 1);
	}

	public static double randDouble(double min, double max) {
		max = min > max ? min : max;

		if (Double.compare(min, max) == 0) {
			return min;
		}

		return min + (max - min) * rand.nextDouble();
	}
	
	public static int[] reversedIntArray(int[] array) {
		int[] reversedArray = new int[array.length];
		
		for (int i = 0; i < array.length; i++) {
			reversedArray[i] = array[array.length - i - 1];
		}
		
		return reversedArray;
	}

	public static void shuffleIntArray(int[] array)
	{	
		for (int i = array.length - 1; i >= 0; i--) {
			int index = rand.nextInt(i + 1);
			
			// Simple swap
			int a = array[index];
			array[index] = array[i];
			array[i] = a;
		}
	}
	
	public static int[] stringArrayToIntArray(String s) {
		String[] elems = s.substring(1, s.length() - 1).split("[ ]*,[ ]*");
		int[] array = new int[elems.length];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = Integer.parseInt(elems[i]);
		}
		
		return array;
	}
	
	public static int[][] stringArrayToIntIntArray(String s) {
		String[] parts = s.substring(1, s.length()-1).split(Pattern.quote("]["));
		int[][] array = new int[parts.length][];
		
		for (int i = 0; i < parts.length; i++) {
			array[i] = stringArrayToIntArray("[" + parts[i] + "]");
		}
		
		return array;
	}
	
	public static String intArrayToString(int[] array) {
		return Arrays.toString(array);
	}
	
	public static String intArrayArrayToString(int[][] array) {
		StringBuilder sb = new StringBuilder();
		
		for (int[] a : array) {
			sb.append(Arrays.toString(a));
		}
		
		return sb.toString();
	}

	public static String copySoundfont(Context context, String soundfontFilename) {
		String fileout = null;

		try {
			AssetManager assetManager = context.getAssets();

			InputStream in = null;
			OutputStream out = null;

			in = assetManager.open(soundfontFilename);
			//			File ext = Environment.getDataDirectory();
			File ext = Environment.getExternalStorageDirectory();
			final String folderName = "/JBEarTraining";
			File folder = new File(ext.getAbsoluteFile() + folderName);

			if (folder.exists() == false) {
				if (folder.mkdirs() == false) {
					return null;
				}
			}

			fileout = ext.getAbsolutePath() + folderName + "/" + soundfontFilename;

			out = new FileOutputStream(fileout);

			copyFile(in, out);

			in.close();
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();

			return null;
		}

		return fileout;
	}

	private static void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while((read = in.read(buffer)) != -1){
			out.write(buffer, 0, read);
		}
	}

	//	private void copyAssets() {
	//		AssetManager assetManager = getAssets();
	//		String[] files = null;
	//		try {
	//			files = assetManager.list("");
	//		} catch (IOException e) {
	//			Log.e("tag", "Failed to get asset file list.", e);
	//		}
	//		for(String filename : files) {
	//			InputStream in = null;
	//			OutputStream out = null;
	//			try {
	//				in = assetManager.open(filename);
	//
	//				File ext = Environment.getExternalStorageDirectory();
	//
	//				String fileout = ext.getAbsolutePath() + "/" + filename;
	//				out = new FileOutputStream(fileout);
	//
	//				copyFile(in, out);
	//				in.close();
	//				in = null;
	//				out.flush();
	//				out.close();
	//				out = null;
	//			} catch(IOException e) {
	//				Log.e("tag", "Failed to copy asset file: " + filename, e);
	//			}       
	//		}
	//	}
}

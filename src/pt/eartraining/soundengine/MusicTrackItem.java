package pt.eartraining.soundengine;

import android.os.Parcel;
import android.os.Parcelable;

public abstract class MusicTrackItem implements Parcelable {
	private double mDuration = 1.0;
	private int mVel = 110;
	private boolean mIsChord = false;

	protected MusicTrackItem(int vel, double duration, boolean isChord) {
		mVel = vel;
		mDuration = duration;
		mIsChord = isChord;
	}

	public double getDuration() { return mDuration; }
	protected void setDuration(double duration) { mDuration = duration; }
	protected void setIsChord(boolean isChord) { mIsChord = isChord; }
	public boolean isChord() { return mIsChord; }
	public int getVel() { return mVel; }
	protected void setVel(int vel) { mVel = vel; }

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected MusicTrackItem(Parcel in) {
		mDuration = in.readDouble();
		mVel = in.readInt();
		mIsChord = (in.readInt() == 0) ? false : true;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(mDuration);
		dest.writeInt(mVel);
		dest.writeInt(mIsChord ? 1 : 0);
	}

	public static final Parcelable.Creator<MusicTrackItem> CREATOR = new Parcelable.Creator<MusicTrackItem>() {
		@Override
		public MusicTrackItem createFromParcel(Parcel source) {
			return null;
		}

		@Override
		public MusicTrackItem[] newArray(int size) {
			return null;
		}
	};
}

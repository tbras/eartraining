package pt.eartraining.soundengine;

import pt.eartraining.app.AppPreferences;
import pt.eartraining.musictheory.Dynamics;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.util.Log;

public class MusicPlayer {
	public static final String PLAYER_THREAD_NAME = "MusicPlayerThread";
	
	private PlayerThread mPlayerThread;

	private static MusicPlayer mInstance = null;

	public static synchronized MusicPlayer getInstance() {
		if (mInstance == null) {
			mInstance = new MusicPlayer();
		}

		return mInstance;
	}

	private MusicPlayer() {
		mPlayerThread = new PlayerThread();
		mPlayerThread.setPriority(Thread.MAX_PRIORITY);
		mPlayerThread.start();
	}

	public void playNote(int note, double seconds) {
		MusicTrack track = new MusicTrack(60);
		track.addNote(note, Dynamics.DYNAMICS_MP.getVel(), seconds);
		
		playTrack(track);
	}

	public void playTrack(MusicTrack track) {
		stopTrack();

		if (track != null) {
			mPlayerThread.playAudio(track);
		}
	}

	//	public void replayTrack() {
	//		mPlayerThread.replayAudio();
	//	}

	public void pauseTrack() {
		mPlayerThread.pauseAudio();
	}

	public void resumeTrack() {
		mPlayerThread.resumeAudio();
	}

	public void stopTrack() {
		mPlayerThread.stopAudio();
	}

	//	public void releasePlayer() {
	//		mPlayerThread.release(); 
	//	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();

		if (mPlayerThread.isAlive()) {
			//			releasePlayer();
		}
	}

	private class PlayerThread extends Thread {
		private static final int STATE_PLAYING = 1;
		private static final int STATE_PAUSED = 2;
		private static final int STATE_STOPPED = 3;

		private final Object mPlayingLock = new Object();
		private final Object mPauseLock = new Object();

		private FluidSynth synth = null;
		private volatile int mPlayState;
		private volatile MusicTrack mTrack;
		private boolean mReleaseThread;

		public PlayerThread() {
			setName(PLAYER_THREAD_NAME);

			mPlayState = STATE_STOPPED;
			mReleaseThread = false;
		}

		@Override
		public void run() {
			AppPreferences prefs = AppPreferences.getInstance();

			synth = FluidSynth.getInstance();

			if (synth == null) { return; }

			synth.setGain(prefs.getPlaybackGain());
			
			int bufferSize = synth.getStreamingAudioTrackMinBufferSize();
			AudioTrack audioTrack = synth.createStreamingAudioTrack();
			audioTrack.play();
			

			while (true) {
				synchronized(mPlayingLock) {
					Log.d("Debug", "Waiting to play...");

					// Waits for playSound
					if (mPlayState == STATE_STOPPED) {
						audioTrack.flush();

						try {
							mPlayingLock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						// Started playing
						mPlayState = STATE_PLAYING;
						
						// Recreate audiotrack if the settings of the synth were refreshed
						if (synth.isDirty()) {
							audioTrack.release();
							
							bufferSize = synth.getStreamingAudioTrackMinBufferSize();
							audioTrack = synth.createStreamingAudioTrack();
							audioTrack.play();
							
							synth.setIsDirty(false);
						}

						if (mReleaseThread) {
							audioTrack.stop();
							audioTrack.release();
							break;
						}
					}

					synth.startSequencer();

					final int MAX_BLOCK_SIZE = bufferSize / 2;
					short[] samples = new short[MAX_BLOCK_SIZE];				
					int samplesLeft = synth.scheduleTrack(mTrack);

					Log.d("Debug", "Playing...");
					while (samples != null && samplesLeft > 0 && mPlayState == STATE_PLAYING) {
						int blockSize = samplesLeft < MAX_BLOCK_SIZE ? samplesLeft : MAX_BLOCK_SIZE;

						synth.writeS16(samples);
						audioTrack.write(samples, 0, blockSize);

						samplesLeft -= blockSize;

						if (mPlayState == STATE_PAUSED) {
							Log.d("Debug", "Pausing...");

							synchronized(mPauseLock) {
								try {
									mPauseLock.wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}

							mPlayState = STATE_PLAYING;

							Log.d("Debug", "Resume playing...");
						}
					}

					audioTrack.flush();

					mPlayState = STATE_STOPPED;

					Log.d("Debug", "Stopped");
				}
			}

			Log.d("Debug", "Finalizing thread...");
		}

		public void playAudio(MusicTrack track) {
			stopAudio();

			if (mPlayState == STATE_STOPPED) {
				synchronized (mPlayingLock) {
					// Changes samples
					mTrack = track;

					mPlayState = STATE_PLAYING;
					mPlayingLock.notifyAll();
				}
			}
		}

		//		public void replayAudio() {
		//			stopAudio();
		//
		//			if (mPlayState == STATE_STOPPED) {
		//				synchronized (mPlayingLock) {
		//					mPlayState = STATE_PLAYING;
		//					mPlayingLock.notifyAll();
		//				}
		//			}
		//		}

		public void pauseAudio() {
			if (mPlayState == STATE_PLAYING) {
				mPlayState = STATE_PAUSED;
			}
		}

		public void resumeAudio() {
			if (mPlayState == STATE_PAUSED) {
				synchronized (mPauseLock) {
					mPlayState = STATE_PLAYING;
					mPauseLock.notifyAll();
				}
			}
		}

		public void stopAudio() {
			if (synth != null) {
				synth.resetFluid();
			}

			if (mPlayState == STATE_PAUSED) {
				resumeAudio();
			}

			if (mPlayState == STATE_PLAYING) {
				mPlayState = STATE_STOPPED;
			}
		}

		public void release() {
			mReleaseThread = true;

			resumeAudio();
		}
	}
}

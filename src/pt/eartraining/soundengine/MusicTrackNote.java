package pt.eartraining.soundengine;

import android.os.Parcel;
import android.os.Parcelable;

public class MusicTrackNote extends MusicTrackItem {
	int mNote;

	public MusicTrackNote(int note, int vel, double duration) {
		super(vel, duration, false);

		mNote = note;
	}

	public int getNote() { return mNote; }

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected MusicTrackNote(Parcel in) {
		super(in);
		mNote = in.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeInt(mNote);
	}

	public static final Parcelable.Creator<MusicTrackNote> CREATOR = new Parcelable.Creator<MusicTrackNote>() {
		public MusicTrackNote createFromParcel(Parcel in) {
			return new MusicTrackNote(in);
		}

		public MusicTrackNote[] newArray(int size) {
			return new MusicTrackNote[size];
		}
	};
}

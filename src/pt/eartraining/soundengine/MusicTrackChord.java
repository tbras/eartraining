package pt.eartraining.soundengine;

import android.os.Parcel;
import android.os.Parcelable;

public class MusicTrackChord extends MusicTrackItem {
	int[] mNotes;
	
	public MusicTrackChord(int root, int[] formula, int vel, double duration) {
		super(vel, duration, true);
		
		mNotes = new int[formula.length];
		
		for (int i = 0; i < formula.length; i++) {
			mNotes[i] = root + formula[i];
		}
	}
	
	public MusicTrackChord(int[] notes, int vel, double duration) {
		super(vel, duration, true);
		
		mNotes = new int[notes.length];

		System.arraycopy(notes, 0, this.mNotes, 0, notes.length);
	}
	
	public int[] getNotes() {
		return mNotes;
	}
	
	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected MusicTrackChord(Parcel in) {
		super(in);
		mNotes = in.createIntArray();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeIntArray(mNotes);
	}
	
	public static final Parcelable.Creator<MusicTrackChord> CREATOR = new Parcelable.Creator<MusicTrackChord>() {
		public MusicTrackChord createFromParcel(Parcel in) {
			return new MusicTrackChord(in);
		}

		public MusicTrackChord[] newArray(int size) {
			return new MusicTrackChord[size];
		}
	};
}

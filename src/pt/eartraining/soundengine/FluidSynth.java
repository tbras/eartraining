package pt.eartraining.soundengine;

import junit.framework.Assert;

import pt.eartraining.app.App;
import pt.eartraining.app.AppPreferences;
import pt.eartraining.app.R;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;
import android.widget.Toast;

// TODO: adicionar sounds ja pre feitos no caso de o o utilizador nao conseguir usar o synth

public class FluidSynth {
	// DON'T CHANGE THESE VALUES. THEY ARE REFERENCED AT jni/soundengine/soundengine.c.
	private static final int ET_FLUIDSYNTH_SUCCESS = 0;
	private static final int ET_FLUIDSYNTH_SETTINGS_FAILED = 1;
	private static final int ET_FLUIDSYNTH_SYNTH_FAILED = 2;
	private static final int ET_FLUIDSYNTH_SOUNDFONT_NOT_FOUND = 3;
	private static final int ET_FLUIDSYNTH_GET_LONG_ARRAY_ELEMENTS_FAILED = 4;
	private static final int ET_FLUIDSYNTH_GET_SHORT_ARRAY_ELEMENTS_FAILED = 5;
	private static final int ET_FLUIDSYNTH_IS_NULL = 6;
	private static final int ET_FLUIDSYNTH_INVALID_PROGRAM = 7;
	private static final int ET_FLUIDSYNTH_SET_POLYPHONY_FAILED = 8;

	// Private constants
	private static final int TRACK_SIZE_CACHE_IN_KILOBYTES = 1024 * 1024 * 1;	// 1 MB

	// Public constants
	public static final int SYNTH_RESOLUTION = 1000;	

	static {  
		System.loadLibrary("glib-2.0");
		System.loadLibrary("gmodule-2.0");
		System.loadLibrary("gthread-2.0");
		System.loadLibrary("gobject-2.0");
		System.loadLibrary("fluidsynth-1.1.6");
		System.loadLibrary("soundengine");
	}  

	// JNI methods
	private native int nInitFluidSynth(String soundFontPath, int sampleRate, int polyphony, long[] out);
	private native int nDeleteFluidSynth(long synth, long settings);
	private native int nResetFluid(long synth);
	private native int nSelectProgram(long synth, int channel, long soundFontId, long bankNum, long presetNum);
	private native int nSetSampleRate(long synth, int sampleRate);
	private native int nNoteOn(long synth, int channel, int key, int vel);
	private native int nNoteOff(long synth, int channel, int key);
	private native int nScheduleNoteOn(long sequencer, long cliendId, int channel, int key, int vel, long tick, boolean absolute);
	private native int nScheduleNoteOff(long sequencer, long cliendId, int channel, int key, long tick, boolean absolute);
	private native int nStartSequencer(long synth, String clientName, long[] out);
	private native int nDeleteSequencer(long sequencer);
	private native int nWriteS16Stereo(long synth, int len, short[] out);
	private native int nWriteS16Mono(long synth, int len, short[] out);
	private native int nPrintProgram(long synth, int channel);
	private native int nSetGain(long synth, float gain);
	private native int nGetGain(long synth, float[] gain);

	private int mSampleRate;
	private long mSynth;
	private long mSettings;
	private long mSoundFontId;
	private long mSequencer;
	private long mCliendId; 
	private boolean mIsStereo = true;
	
	private boolean mIsDirty = false;

	private static FluidSynth mInstance = null;
	
	public static synchronized FluidSynth getInstance() {
		if (mInstance == null) {
			// IMPORTANT: FluidSynth cannot be called outside MusicPlayer.PlayerThread!!! (maybe fix in future)
			// because ALL synth calls must be made from the thread that initialized the synth.
			boolean condition = Thread.currentThread().getName().equals(MusicPlayer.PLAYER_THREAD_NAME);
			
			Assert.assertTrue("FluidSynth cannot be called from any thread but " + MusicPlayer.PLAYER_THREAD_NAME, condition);
			
			AppPreferences prefs = AppPreferences.getInstance();
			
			mInstance = new FluidSynth(prefs.getSoundFontPath(), prefs.getSampleRate(), prefs.getStereo());
		}
		
		return mInstance;
	}
	
	public boolean refreshSettings() {
		mIsDirty = true;
		
		return setupFluidSynth(
				AppPreferences.getInstance().getSoundFontPath(),
				AppPreferences.getInstance().getSampleRate(),
				AppPreferences.getInstance().getStereo());
	}
	
	private FluidSynth(String soundFontPath, int sampleRate, boolean stereo) {
		setupFluidSynth(soundFontPath, sampleRate, stereo);
	}
	
	private boolean setupFluidSynth(String soundFontPath, int sampleRate, boolean stereo) {
		long out[] = new long[3];

		int rc = nInitFluidSynth(soundFontPath, sampleRate, 16, out);

		if (rc == ET_FLUIDSYNTH_SETTINGS_FAILED) {
			showErrorDialog(App.getContext().getString(R.string.alert_dialog_fluidsynth_error_init_fail));
		} else if (rc == ET_FLUIDSYNTH_SYNTH_FAILED) {
			showErrorDialog(App.getContext().getString(R.string.alert_dialog_fluidsynth_error_init_fail));
		} else if (rc == ET_FLUIDSYNTH_SOUNDFONT_NOT_FOUND) {
			showErrorDialog(App.getContext().getString(R.string.alert_dialog_fluidsynth_error_sf_not_found) + 
					"'" + soundFontPath + "'");
		} else if (rc == ET_FLUIDSYNTH_SET_POLYPHONY_FAILED) {
			showErrorDialog(App.getContext().getString(R.string.alert_dialog_fluidsynth_error_init_fail));
		}

		mSynth = out[0];
		mSettings = out[1];
		mSoundFontId = out[2];
		mSampleRate = sampleRate;
		mIsStereo = stereo;
		
		setGain(AppPreferences.getInstance().getPlaybackGain());
		
		if (rc == ET_FLUIDSYNTH_SUCCESS) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isDirty() {
		return mIsDirty;
	}
	
	public void setIsDirty(boolean isDirty) {
		mIsDirty = isDirty;
	}

	public void resetFluid() {
		nResetFluid(mSynth);
	}

	public void startSequencer() {
		if (mSequencer != 0) {
			nDeleteSequencer(mSequencer);
		}

		long out[] = new long[2];

		nStartSequencer(mSynth, "MySequencer", out);

		mSequencer = out[0];
		mCliendId = out[1];

		if (mSequencer == 0) {
			Log.e("Error", "Failed starting sequencer.");
		}
	}

	public void deleteSequencer() {
		if (mSequencer != 0) {
			nDeleteSequencer(mSequencer);
			mSequencer = 0;
		}
	}

	/**
	 * Set the gain
	 * @param gain clamps values outside [0.0, 10.0]
	 */
	public void setGain(double gain) {
		nSetGain(mSynth, (float)gain);
	}

	public double getGain() {
		float[] gain = new float[1];

		nGetGain(mSynth, gain);

		return (double)gain[0];
	}

	private void selectProgram(int channel, long bankNum, long presetNum){
		if (nSelectProgram(mSynth, channel, mSoundFontId, bankNum, presetNum) == ET_FLUIDSYNTH_INVALID_PROGRAM) {
			// TODO: implement
		}
	}

	public void noteOn(int channel, int key, int vel) {
		nNoteOn(mSynth, channel, key, vel);
	}

	public void noteOff(int channel, int key) {
		nNoteOff(mSynth, channel, key);
	}

	public void scheduleNoteOn(int channel, int key, int vel, int tick) {
		nScheduleNoteOn(mSequencer, mCliendId, channel, key, vel, tick, true);
	}

	public void scheduleNoteOff(int channel, int key, int tick) {
		nScheduleNoteOff(mSequencer, mCliendId, channel, key, tick, true);
	}

	public void writeS16(short[] out) {
		if (mIsStereo) {
			nWriteS16Stereo(mSynth, out.length, out);
		} else {
			nWriteS16Mono(mSynth, out.length, out);
		}
	}

	/**
	 * Schedule <code>track</code> to the current sequencer. <br/><br/>
	 * 
	 * Example of usage:<br/><br/>
	 * <code>startSequencer();</code><br/><br/>
	 * <code>int samplesSize = scheduleTrack(track);</code><br/><br/>
	 * <code>... writeS16 and audio track write (in another thread)</code><br/><br/>
	 * <code>deleteSequencer();</code>
	 * 
	 * @param track
	 * @return How many samples (16 bits - short) were scheduled.
	 */
	public int scheduleTrack(MusicTrack track) {
		int samplesLen = (int)((float)this.mSampleRate * track.getTotalTempoInSeconds() * (mIsStereo ? 2 : 1));
		int ticksPerBeat = (int)(1000.0f * (60.0 / (float)track.getTempoInBPM()));
		int tick = 0;

		for (MusicTrackItem item : track.getItems()) {
			int channel = track.getChannel();

			int[] notes = item.isChord() ? ((MusicTrackChord)item).getNotes() 
					: new int[]{((MusicTrackNote)item).getNote()};

			for (int i = 0; i < notes.length; i++) {
				scheduleNoteOn(channel, notes[i], item.getVel(), tick);

				// Only schedule noteOff if the sustain pedal is not pressed
				if (true) {
					int nextTick = tick + (int)((item.getDuration() * ticksPerBeat));

					scheduleNoteOff(channel,  notes[i], nextTick);
				}

			}


			tick += (int)(item.getDuration() * ticksPerBeat);
		}

		return samplesLen;
	}

	public short[] loadMusicTrack(MusicTrack track) {
		// Start sequence
		startSequencer();

		int samplesLen = scheduleTrack(track);

		short[] samples = new short[samplesLen];

		writeS16(samples);

		deleteSequencer();

		return samples;
	}

	public byte[] loadMusicTrackInBytes(MusicTrack track) {
		// Start sequence
		startSequencer();

		int samplesLen = scheduleTrack(track);

		short[] samples = new short[samplesLen];

		writeS16(samples);

		deleteSequencer();

		byte[] samplesInBytes = new byte[samples.length * 2];

		for (int i = 0; i < samples.length; i++) {
			samplesInBytes[i * 2] = (byte)(samples[i] & 0xff);
			samplesInBytes[(i * 2) + 1] = (byte)((samples[i] >> 8) & 0xff);
		}

		return samplesInBytes;
	}
	
	public int getStreamingAudioTrackMinBufferSize() {
		int channelConfig = mIsStereo ? AudioFormat.CHANNEL_OUT_STEREO : AudioFormat.CHANNEL_OUT_MONO;
		
		return AudioTrack.getMinBufferSize(mSampleRate,
				channelConfig, AudioFormat.ENCODING_PCM_16BIT);
	}

	public AudioTrack createStreamingAudioTrack() {
		int channelConfig = mIsStereo ? AudioFormat.CHANNEL_OUT_STEREO : AudioFormat.CHANNEL_OUT_MONO;
		int bufferSize = AudioTrack.getMinBufferSize(mSampleRate,
				channelConfig, AudioFormat.ENCODING_PCM_16BIT);

		AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
				mSampleRate, channelConfig, AudioFormat.ENCODING_PCM_16BIT, 
				bufferSize, AudioTrack.MODE_STREAM);

		// TODO: JNI try setting a greater gain
		audioTrack.setStereoVolume(15, 15);

		audioTrack.play();

		return audioTrack;
	}

	public void release() {
		deleteSequencer();

		nDeleteFluidSynth(mSynth, mSettings);

		//		try {
		//			if (mPlayerThread != null) {
		//				mPlayerThread.join(1000);
		//			}
		//		} catch (InterruptedException e) {
		//			Log.i("Info", "Thread did not ended gracefully.");
		//		}
	}

	private void showErrorDialog(String message) {
//		Toast.makeText(App.getContext(), message, Toast.LENGTH_LONG).show();
		
//		new AlertDialog.Builder(App.getContext())
//		.setTitle(App.getContext().getString(R.string.alert_dialog_fluidsynth_error_title))
//		.setMessage(message)
//		.setIcon(android.R.drawable.ic_dialog_alert)
//		.setPositiveButton(android.R.string.yes, null).show();
	}
}

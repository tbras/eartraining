package pt.eartraining.soundengine;

import pt.eartraining.app.AppPreferences;
import pt.eartraining.app.JBUtils;
import pt.eartraining.musictheory.Dynamics;
import pt.eartraining.musictheory.Interval;
import pt.eartraining.musictheory.IntervalType;

public class MusicTrackFactory {
	private static double FADE_OUT_DURATION = 3.0;
	
	private int mTempoInBPM;
	private Dynamics mDynamics;
	
	public MusicTrackFactory() {
		mTempoInBPM = AppPreferences.getInstance().getTempoInBPM();
		mDynamics = Dynamics.DYNAMICS_MP;
	}
	
	private double durationInSecondsForBPM(int bpm, double duration) {
		return ((double)bpm * duration) / 60.0;
	}
	
	private MusicTrack newTrack(int root, int[] formula, boolean arpeggiated, boolean fadeOut) {
		MusicTrack track = new MusicTrack(mTempoInBPM);
		
		double fadeOutDuration =  fadeOut ? durationInSecondsForBPM(mTempoInBPM, FADE_OUT_DURATION) : 0.0;
		
		if (arpeggiated) {
			for (int i = 0; i < formula.length; i++) {
				// Add fade out to the last note
				if (fadeOut && (i == formula.length - 1)) {
					track.addNote(root + formula[i], mDynamics.getVel(), 1.0 + fadeOutDuration);
				} else {
					track.addNote(root + formula[i], mDynamics.getVel(), 1.0);
				}
			} 
		} else {			
			track.addChord(root, formula, mDynamics.getVel(), 1.0 + fadeOutDuration);
		}
		
		return track;
	}
	
	public MusicTrack newIntervalIdentificationTrack(int root, Interval interval, IntervalType type, boolean fadeOut) {
		if (type == IntervalType.MELODIC_ASCENDING) {
			return newTrack(root, interval.getFormula(), true, fadeOut);
		} else if (type == IntervalType.MELODIC_DESCENDING) {
			return newTrack(root, JBUtils.reversedIntArray(interval.getFormula()), true, fadeOut);
		} else {
			return newTrack(root, interval.getFormula(), false, true);
		}
	}
	
	public MusicTrack newIntervalComparisonTrack(int root, Interval firstInterval, IntervalType firstType,
			Interval secondInterval, IntervalType secondType) {
		MusicTrack track = newIntervalIdentificationTrack(root, firstInterval, firstType, false);
		track.appendTrack(newIntervalIdentificationTrack(root, secondInterval, secondType, false), 0.2);
		
		return track;
	}
	
	public MusicTrack newMelodicDictationTrack(int[] notes) {
		MusicTrack track = new MusicTrack(mTempoInBPM);
		
		track.addNotes(notes, mDynamics.getVel(), 1.0);
		
		return track;
	}
}

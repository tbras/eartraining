package pt.eartraining.soundengine;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import android.os.Parcel;
import android.os.Parcelable;

import pt.eartraining.musictheory.Dynamics;


public class MusicTrack implements Parcelable {
	private static final AtomicInteger counter = new AtomicInteger(0);

	private int mTempoInBPM;
	private int mChannel;
	private String mID = String.valueOf(counter.getAndIncrement());

	private ArrayList<MusicTrackItem> mItems = new ArrayList<MusicTrackItem>();

	public MusicTrack(int tempoInBPM) {
		this(tempoInBPM, 0);
	}

	public MusicTrack(int tempoInBPM, int channel) {		
		this.mTempoInBPM = tempoInBPM;
		this.mChannel = channel;
	}

	public MusicTrack(int tempoInBPM, ArrayList<MusicTrackItem> trackItems) {
		this(tempoInBPM, 0, trackItems);
	}

	public MusicTrack(int tempoInBPM, int channel, ArrayList<MusicTrackItem> trackItems) {
		this.mTempoInBPM = tempoInBPM;
		this.mChannel = channel;

		addTrackItems(trackItems);
	}

	// Helpers
	public void addTrackItems(ArrayList<MusicTrackItem> trackItems) {
		mItems.addAll(trackItems);
	}

	public void addTrackItem(MusicTrackItem trackItem) {
		mItems.add(trackItem);
	}

	public void addNote(int note) {
		mItems.add(new MusicTrackNote(note, Dynamics.DYNAMICS_MF.getVel(), 1.0));
	}

	public void addNote(int note, int vel, double duration) {
		mItems.add(new MusicTrackNote(note, vel, duration));
	}

	public void addChord(int[] notes, int vel, double duration) {
		mItems.add(new MusicTrackChord(notes, vel, duration));
	}

	public void addChord(int root, int[] formula, int vel, double duration) {
		mItems.add(new MusicTrackChord(root, formula, vel, duration));
	}

	public void addChord(int root, int[] formula, int vel, double duration, boolean arpeggiated) {
		if (arpeggiated) {
			for (int f : formula) {
				addNote(root + f, vel, duration / (double)formula.length);
			} 
		} else {
			mItems.add(new MusicTrackChord(root, formula, vel, duration));
		}
	}

	public void addNotes(int[] notes, int vel, double individualNoteDuration) {
		for (int i = 0; i < notes.length; i++) {
			mItems.add(new MusicTrackNote(notes[i], vel, individualNoteDuration));
		}
	}

	public void addSilence(double duration) {
		if (Double.compare(duration, 0.0) > 0) {  
			addNote(0, 0, duration);
		}
	}

	public void appendTrack(MusicTrack track, double pauseDuration) {
		addSilence(pauseDuration);
		mItems.addAll(track.getItems());
	}

	public double getTotalTempoInSeconds() {
		double total = 0.0;

		for (MusicTrackItem item : mItems) {
			total += (item.getDuration() * (60.0 / (double)mTempoInBPM));
		}

		return total; 
	}

	// Getters and setters
	public int getTempoInBPM() {
		return mTempoInBPM;
	}
	public int getChannel() {
		return mChannel;
	}

	public ArrayList<MusicTrackItem> getItems() {
		return mItems;
	}

	public String getID() {
		return mID;
	}

	//	public void shapePhrase(int initialVel, int finalVel) {
	//		// TODO: implement
	//	}

	

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected MusicTrack(Parcel in) {
		mTempoInBPM = in.readInt();
		mChannel = in.readInt();
		mID = in.readString();
		in.readTypedList(mItems, MusicTrackItem.CREATOR);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(mTempoInBPM);
		dest.writeInt(mChannel);
		dest.writeString(mID);
		dest.writeTypedList(mItems);
	}

	@Override
	public int describeContents() {
		return 0;
	}
	
	public static final Parcelable.Creator<MusicTrack> CREATOR = new Parcelable.Creator<MusicTrack>() {
		public MusicTrack createFromParcel(Parcel in) {
			return new MusicTrack(in);
		}

		public MusicTrack[] newArray(int size) {
			return new MusicTrack[size];
		}
	};
}

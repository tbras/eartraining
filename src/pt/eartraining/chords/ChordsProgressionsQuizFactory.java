package pt.eartraining.chords;

import pt.eartraining.app.JBUtils;
import pt.eartraining.core.ButtonsQuiz;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.core.KeyboardQuiz;
import pt.eartraining.core.QuizFactory;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.musictheory.Dynamics;
import pt.eartraining.soundengine.MusicTrack;
import android.os.Parcel;
import android.os.Parcelable;

public class ChordsProgressionsQuizFactory extends QuizFactory {
	protected static int DYNAMYCS = Dynamics.DYNAMICS_MF.getVel();
	
	public ChordsProgressionsQuizFactory(QuizSpecification quizSpec) {
		super(quizSpec);

		setLowestNote(45); 	// A
		setHighestNote(55);	// G bellow middle C
	}

	@Override
	public ButtonsQuiz nextButtonQuiz() {
		ChordsProgressionsSpecification spec = (ChordsProgressionsSpecification) getQuizSpec();

		int root = getRandomRootNote(0);

		ButtonQuizAnswer[] answers = new ButtonQuizAnswer[spec.getChordProgressions().length];

		for (int i = 0; i < answers.length; i++) {	
			MusicTrack track = new MusicTrack(getTempoInBPM());

			int[][] formula = spec.getChordProgressions()[i].getFormula();
			for (int j = 0; j < formula.length - 1; j++) {
				track.addChord(root, formula[j], DYNAMYCS, 1.5, spec.isArpeggio());
			}
			track.addChord(root, formula[formula.length-1], DYNAMYCS, 2.5, spec.isArpeggio());

			answers[i] = new ButtonQuizAnswer(spec.getChordProgressions()[i].ordinal(), 
					spec.getChordProgressions()[i].getName(), track);
		}

		ButtonQuizAnswer rightAnswer = answers[JBUtils.randInt(0, answers.length-1)];

		return new ButtonsQuiz(rightAnswer.getTrack(), answers, rightAnswer.getId());
	}

	@Override
	public KeyboardQuiz nextKeyboardQuiz() {
		// TODO Auto-generated method stub
		return null;
	}

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected ChordsProgressionsQuizFactory(Parcel in) {
		super(in);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	public static final Parcelable.Creator<ChordsProgressionsQuizFactory> CREATOR = new Parcelable.Creator<ChordsProgressionsQuizFactory>() {
		@Override
		public ChordsProgressionsQuizFactory createFromParcel(Parcel in) {
			return new ChordsProgressionsQuizFactory(in);
		}

		@Override
		public ChordsProgressionsQuizFactory[] newArray(int size) {
			return new ChordsProgressionsQuizFactory[size];
		}
	};
}
package pt.eartraining.chords;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pt.eartraining.app.R;
import pt.eartraining.core.DatabaseHelper.Column;
import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.SpecificationsDataSource;
import pt.eartraining.musictheory.Chord;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ChordsIdentificationDataSource extends SpecificationsDataSource {
	private static final String COLUMN_CHORDS = "chords";
	private static final String COLUMN_ARPEGGIO = "arpeggio";
	private final String[] COLUMN_NAMES = { COLUMN_ID, COLUMN_SUBJECT,
			COLUMN_TITLE, COLUMN_SUBTITLE, COLUMN_VALIDATION_COUNT, 
			COLUMN_CHORDS, COLUMN_ARPEGGIO };

	@Override
	public ArrayList<QuizSpecification> getSpecifications(
			SQLiteDatabase db, boolean custom) {
		ArrayList<QuizSpecification> specs = new ArrayList<QuizSpecification>();

		Cursor cursor = db.query(dataSourceName(), 
				COLUMN_NAMES,  COLUMN_CUSTOM + " = " + (custom ? 1 : 0), 
				null, null, null, COLUMN_ID);

		while (cursor.moveToNext()) {
			ChordsIdentificationSpecification spec = new ChordsIdentificationSpecification();
			spec.setId(cursor.getLong(0));
			spec.setSubject(EarTrainingSubject.fromInt(cursor.getInt(1)));
			spec.setTitle(cursor.getString(2));
			spec.setSubtitle(cursor.getString(3));
			spec.setValidationCount(cursor.getInt(4));
			spec.setChords(stringToChordArray(cursor.getString(5)));
			spec.setArpeggio(cursor.getInt(6) != 0 ? true : false);

			specs.add(spec);
		}

		return specs;
	}

	@Override
	public boolean addCustomSpec(SQLiteDatabase db, QuizSpecification spec) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String dataSourceName() {
		return "chords_identification";
	}

	@Override
	public Column[] dataSourceColumns() {
		Column[] columns = new Column[] {
				new Column(COLUMN_ID, Column.ColumnType.INTEGER, true),
				new Column(COLUMN_SUBJECT, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_CUSTOM, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_TITLE, Column.ColumnType.TEXT, false),
				new Column(COLUMN_SUBTITLE, Column.ColumnType.TEXT, false),
				new Column(COLUMN_VALIDATION_COUNT, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_CHORDS, Column.ColumnType.TEXT, false),
				new Column(COLUMN_ARPEGGIO, Column.ColumnType.INTEGER, false)
		};

		return columns;
	}

	@Override
	public void loadDefaultData(SQLiteDatabase db) {
		JSONArray specs = loadJSONFile(R.raw.chords_identification);

		try {
			db.beginTransaction();

			for (int i = 0; i < specs.length(); i++) {
				JSONObject spec = specs.getJSONObject(i);

				boolean arpeggio = spec.optBoolean(COLUMN_ARPEGGIO, false);

				ContentValues values = new ContentValues();
				values.put(COLUMN_TITLE, spec.optString(COLUMN_TITLE, "Not Set"));
				values.put(COLUMN_SUBTITLE, arpeggio ? "Arpeggio" : "Chord");
				values.put(COLUMN_SUBJECT, getSubject().ordinal());
				values.put(COLUMN_CUSTOM, 0);
				values.put(COLUMN_VALIDATION_COUNT, 10);
				values.put(COLUMN_CHORDS, spec.getString(COLUMN_CHORDS));
				values.put(COLUMN_ARPEGGIO, arpeggio ? 1 : 0);

				// Insert row into database
				long id = db.insert(dataSourceName(), null, values);

				if (id == -1) {
					Log.e("Error", "Could not insert '" + values.toString() + "'");
				} else {
					Log.i("DBInfo", "Inserted interval with id = " + id);
				}
			}

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public EarTrainingSubject getSubject() {
		return EarTrainingSubject.ChordsIdentification;
	}

	private String chordArrayToString(Chord[] array) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < array.length; i++) {
			sb.append(array[i].toString());

			if (i < array.length - 1) {
				sb.append(",");
			}
		}

		return sb.toString();
	}

	private Chord[] stringToChordArray(String stringArray) {
		String[] elems = stringArray.split("[ ]*,[ ]*");
		Chord[] chords = new Chord[elems.length];

		for (int i = 0; i < elems.length; i++) {
			chords[i] = Chord.valueOf(elems[i]);
		}

		return chords;
	}
}

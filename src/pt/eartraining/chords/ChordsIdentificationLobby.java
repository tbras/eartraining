package pt.eartraining.chords;

import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.QuizLobbyActivity;
import pt.eartraining.core.SpecificationDialogFragment;
import pt.eartraining.core.SpecificationsDataSource;


public class ChordsIdentificationLobby extends QuizLobbyActivity {

	@Override
	protected SpecificationDialogFragment getSpecDialogFragment() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected SpecificationsDataSource getSpecsDataSource() {
		return new ChordsIdentificationDataSource();
	}

	@Override
	protected Class<?> getQuizFactoryClass() {
		return ChordsIdentificationQuizFactory.class;
	}

	@Override
	protected EarTrainingSubject getSubject() {
		return EarTrainingSubject.ChordsIdentification;
	}
}

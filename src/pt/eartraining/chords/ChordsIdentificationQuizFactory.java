package pt.eartraining.chords;

import pt.eartraining.app.JBUtils;
import pt.eartraining.core.ButtonsQuiz;
import pt.eartraining.core.KeyboardQuiz;
import pt.eartraining.core.QuizFactory;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.musictheory.Chord;
import pt.eartraining.musictheory.Dynamics;
import pt.eartraining.soundengine.MusicTrack;
import android.os.Parcel;
import android.os.Parcelable;

public class ChordsIdentificationQuizFactory extends QuizFactory {
	protected static int DYNAMYCS = Dynamics.DYNAMICS_MF.getVel();

	private int mChordMaxSpan = 5;

	public ChordsIdentificationQuizFactory(QuizSpecification quizSpec) {
		super(quizSpec);

		Chord[] chords = ((ChordsIdentificationSpecification) quizSpec).getChords();

		// Calculate the max span between root and highest note
		for (Chord c : chords) {
			int last = c.getFormula()[c.getFormula().length - 1];

			if (last > mChordMaxSpan) {
				mChordMaxSpan = last;
			}
		}
	}

	@Override
	public ButtonsQuiz nextButtonQuiz() {
		ChordsIdentificationSpecification spec = (ChordsIdentificationSpecification) getQuizSpec();

		int root = getRandomRootNote(mChordMaxSpan);

		ButtonQuizAnswer[] answers = new ButtonQuizAnswer[spec.getChords().length];

		for (int i = 0; i < answers.length; i++) {
			MusicTrack track = new MusicTrack(getTempoInBPM());
			track.addChord(root, spec.getChords()[i].getFormula(), DYNAMYCS, 4.0, spec.isArpeggio());

			answers[i] = new ButtonQuizAnswer(spec.getChords()[i].ordinal(), 
					spec.getChords()[i].getShortName(), track);
		}

		ButtonQuizAnswer rightAnswer = answers[JBUtils.randInt(0, answers.length-1)];

		return new ButtonsQuiz(rightAnswer.getTrack(), answers, rightAnswer.getId());
	}

	@Override
	public KeyboardQuiz nextKeyboardQuiz() {
		// TODO Auto-generated method stub
		return null;
	}

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected ChordsIdentificationQuizFactory(Parcel in) {
		super(in);

		mChordMaxSpan = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		
		dest.writeInt(mChordMaxSpan);
	}

	public static final Parcelable.Creator<ChordsIdentificationQuizFactory> CREATOR = new Parcelable.Creator<ChordsIdentificationQuizFactory>() {
		@Override
		public ChordsIdentificationQuizFactory createFromParcel(Parcel in) {
			return new ChordsIdentificationQuizFactory(in);
		}

		@Override
		public ChordsIdentificationQuizFactory[] newArray(int size) {
			return new ChordsIdentificationQuizFactory[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + mChordMaxSpan;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChordsIdentificationQuizFactory other = (ChordsIdentificationQuizFactory) obj;
		if (mChordMaxSpan != other.mChordMaxSpan)
			return false;
		return true;
	}
}
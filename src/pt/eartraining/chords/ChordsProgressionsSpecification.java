package pt.eartraining.chords;

import java.util.Arrays;

import android.os.Parcel;
import android.os.Parcelable;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.musictheory.ChordProgression;

public class ChordsProgressionsSpecification extends QuizSpecification {
	private ChordProgression[] mChordProgressions;
	private boolean mArpeggio;

	public ChordsProgressionsSpecification() {
		super();
	}

	public ChordProgression[] getChordProgressions() { return mChordProgressions; }
	public void setChordProgressions(ChordProgression[] chordProgressions) { mChordProgressions = chordProgressions; }
	public boolean isArpeggio() { return mArpeggio; }
	public void setArpeggio(boolean arpeggio) { mArpeggio = arpeggio; }
	
	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected ChordsProgressionsSpecification(Parcel in) {
		super(in);
		
		int[] chordProgressions = in.createIntArray();
		mChordProgressions = new ChordProgression[chordProgressions.length];
		for (int i = 0; i < chordProgressions.length; i++) {
			mChordProgressions[i] = ChordProgression.fromInt(chordProgressions[i]);
		}

		mArpeggio = (in.readInt() == 0) ? false : true;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		
		int[] chordProgressions = new int[mChordProgressions.length];
		for (int i = 0; i < chordProgressions.length; i++) {
			chordProgressions[i] = mChordProgressions[i].ordinal();
		}

		dest.writeIntArray(chordProgressions);
		dest.writeInt(mArpeggio ? 1 : 0);
	}

	public static final Parcelable.Creator<ChordsProgressionsSpecification> CREATOR = new Parcelable.Creator<ChordsProgressionsSpecification>() {
		@Override
		public ChordsProgressionsSpecification createFromParcel(Parcel in) {
			return new ChordsProgressionsSpecification(in);
		}

		@Override
		public ChordsProgressionsSpecification[] newArray(int size) {
			return new ChordsProgressionsSpecification[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (mArpeggio ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(mChordProgressions);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChordsProgressionsSpecification other = (ChordsProgressionsSpecification) obj;
		if (mArpeggio != other.mArpeggio)
			return false;
		if (!Arrays.equals(mChordProgressions, other.mChordProgressions))
			return false;
		return true;
	}
}

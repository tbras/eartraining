package pt.eartraining.chords;

import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.QuizLobbyActivity;
import pt.eartraining.core.SpecificationDialogFragment;
import pt.eartraining.core.SpecificationsDataSource;

public class ChordsProgressionsLobby extends QuizLobbyActivity {

	@Override
	protected SpecificationDialogFragment getSpecDialogFragment() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected SpecificationsDataSource getSpecsDataSource() {
		return new ChordsProgressionsDataSource();
	}

	@Override
	protected Class<?> getQuizFactoryClass() {
		return ChordsProgressionsQuizFactory.class;
	}

	@Override
	protected EarTrainingSubject getSubject() {
		return EarTrainingSubject.ChordsProgression;
	}
}

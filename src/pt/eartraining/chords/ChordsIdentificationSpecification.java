package pt.eartraining.chords;

import java.util.Arrays;

import android.os.Parcel;
import android.os.Parcelable;

import pt.eartraining.core.QuizSpecification;
import pt.eartraining.musictheory.Chord;

public class ChordsIdentificationSpecification extends QuizSpecification {
	private Chord[] mChords;
	private boolean mArpeggio;

	public ChordsIdentificationSpecification() {
		super();
	}

	public Chord[] getChords() { return mChords; }
	public void setChords(Chord[] chords) { mChords = chords; }
	public boolean isArpeggio() { return mArpeggio; }
	public void setArpeggio(boolean arpeggio) { mArpeggio = arpeggio; }

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected ChordsIdentificationSpecification(Parcel in) {
		super(in);
		
		int[] chords = in.createIntArray();
		mChords= new Chord[chords.length];
		for (int i = 0; i < chords.length; i++) {
			mChords[i] = Chord.fromInt(chords[i]);
		}
		
		mArpeggio = (in.readInt() == 0) ? false : true;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		
		int[] chords = new int[mChords.length];
		for (int i = 0; i < chords.length; i++) {
			chords[i] = mChords[i].ordinal();
		}

		dest.writeIntArray(chords);
		dest.writeInt(mArpeggio ? 1 : 0);
	}

	public static final Parcelable.Creator<ChordsIdentificationSpecification> CREATOR = new Parcelable.Creator<ChordsIdentificationSpecification>() {
		@Override
		public ChordsIdentificationSpecification createFromParcel(Parcel in) {
			return new ChordsIdentificationSpecification(in);
		}

		@Override
		public ChordsIdentificationSpecification[] newArray(int size) {
			return new ChordsIdentificationSpecification[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (mArpeggio ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(mChords);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChordsIdentificationSpecification other = (ChordsIdentificationSpecification) obj;
		if (mArpeggio != other.mArpeggio)
			return false;
		if (!Arrays.equals(mChords, other.mChords))
			return false;
		return true;
	}
}

package pt.eartraining.custom;

import pt.eartraining.app.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainScreenCardView extends LinearLayout {
	public MainScreenCardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		LayoutInflater inflater = LayoutInflater.from(context);
		
		View v = inflater.inflate(R.layout.card_main_screen, this);
		v.setBackgroundResource(R.drawable.card_background);
		
		TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.CardAttributes);	
		TextView title = (TextView)findViewById(R.id.title);
		title.setText(t.getString(getResources().getInteger(R.integer.CardAttributes_text_index)));
		t.recycle();
	}
}

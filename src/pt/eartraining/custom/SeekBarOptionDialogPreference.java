package pt.eartraining.custom;

import pt.eartraining.app.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class SeekBarOptionDialogPreference extends DialogPreference {
	Number mMin;
	Number mMax;
	Number mDefaultValue;
	Number mCurrentValue;
	Class mType;
	
	public SeekBarOptionDialogPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.SeekBarOptionDialogAttributes);
		String type = t.getString(context.getResources().getInteger(R.integer.SeekBarOptionDialogAttributes_valueType_index));
		String min = t.getString(context.getResources().getInteger(R.integer.SeekBarOptionDialogAttributes_min_index));
		String max = t.getString(context.getResources().getInteger(R.integer.SeekBarOptionDialogAttributes_max_index));
		String defaultValue = t.getString(context.getResources().getInteger(R.integer.SeekBarOptionDialogAttributes_defaultValue_index));
		
		if (type.equals("Integer")) {
			mType = Integer.class;
			mMin = Integer.valueOf(min);
			mMax = Integer.valueOf(max);
			
		} else {
			mType = Double.class;
		}
		
		

		t.recycle();
	}

}

package pt.eartraining.custom;

import java.util.ArrayList;
import java.util.Iterator;

import pt.eartraining.app.R;
import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class QuizButtonsView extends LinearLayout {
	private static final SparseArray<int[]> COLUMNS_BY_ROW = new SparseArray<int[]>();

	private OnButtonsListener mQuizButtonListener;
	ArrayList<Button> mAnswerButtons;
	private int mRightAnswer;
	private boolean mUserHasAnswered = false;
	
	private Button mStartButton;

	public QuizButtonsView(Context context) {
		this(context, null);
	}

	public QuizButtonsView(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.custom_quiz_buttons, this);	
		
		mStartButton = (Button) findViewById(R.id.btn_start);
		mStartButton.setOnClickListener(mStartButtonClickListener);
	}

	public int getRightAnswerKey() {
		return mRightAnswer;
	}

	public void nextQuestion(int rightAnswer) {
		clearResults();

		mRightAnswer = rightAnswer;
		mUserHasAnswered = false;
	}

	public void setOnQuizButtonClickListener(OnButtonsListener listener) {
		mQuizButtonListener = listener;
	}

	private OnClickListener mStartButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (mQuizButtonListener != null) {
				QuizButtonsView.this.removeView(mStartButton);

				setupButtonsGridView(mQuizButtonListener.onStart());
				
				mQuizButtonListener.onButtonsViewCreated();
			}
		}
	};
	
	public void startOver() {
		removeAllViews();
		setWeightSum(1);
		addView(mStartButton);
	}

	private OnClickListener mQuizButtonsClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int key = ((Integer) v.getTag()).intValue();

			if (mQuizButtonListener != null) {
				if (key == mRightAnswer) {
					mQuizButtonListener.onQuizButtonClick(key, true, mUserHasAnswered);
				} else {
					mQuizButtonListener.onQuizButtonClick(key, false, mUserHasAnswered);
				}
			}

			if (!mUserHasAnswered) {
				if (key == mRightAnswer) {
					// TODO: play success sfx
					showResults(true);
				} else {
					// TODO: play failure sfx
					showResults(false);
				}

				mUserHasAnswered = true;
			}
		}
	};

	private void setupButtonsGridView(ArrayList<QuizButton> buttons) {
		LinearLayout optionsContainer = this;
		Context context = getContext();
		LayoutInflater inflater = LayoutInflater.from(context);

		int[] columnsByRow = COLUMNS_BY_ROW.get(buttons.size(), new int[]{});

		mAnswerButtons = new ArrayList<Button>();

		// Setup buttons view in a grid like way
		optionsContainer.setWeightSum(columnsByRow.length);
		optionsContainer.addView((View) inflater.inflate(R.layout.separator_horizontal, optionsContainer, false));
		for (int r = 0; r < columnsByRow.length; r++) {
			LinearLayout line = new LinearLayout(context);
			line.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1));

			for (int c = 0; c < columnsByRow[r]; c++) {
				Button b = (Button) inflater.inflate(R.layout.button_quiz_vertical, line, false);
				line.addView(b);

				// Add vertical separators only on non-last columns
				if (c < columnsByRow[r] - 1) {
					line.addView(inflater.inflate(R.layout.separator_vertical, line, false));
				}

				mAnswerButtons.add(b);
			}

			optionsContainer.addView(line);

			// Add horizontal separators only on non-last) rows
			if (r < columnsByRow.length - 1) {
				optionsContainer.addView((View) inflater.inflate(R.layout.separator_horizontal, optionsContainer, false));
			}	
		}

		// Initialize buttons texts and stuff...
		for (int i = 0; i < mAnswerButtons.size(); i++) {
			Button button = mAnswerButtons.get(i);
			button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
			button.setText(buttons.get(i).getLabel());
			button.setOnClickListener(mQuizButtonsClickListener);
			button.setTag(Integer.valueOf(buttons.get(i).getKey()));
		}
	}

	private void showResults(boolean onlyRightAnswer) {
		int rightColor = getResources().getColor(R.color.answer_right);
		int wrongColor = getResources().getColor(R.color.answer_wrong);

		for (Button button : mAnswerButtons) {
			int key = ((Integer) button.getTag()).intValue();

			if (onlyRightAnswer) {
				if (key == mRightAnswer) {
					button.setBackgroundColor(rightColor);
				}
			} else {
				button.setBackgroundColor((key == mRightAnswer) ? rightColor : wrongColor);
			}
		}
	}

	private void clearResults() {
		for (Button button : mAnswerButtons) {
			// FIXME: this won't work if I change to themes/styles (always white)
			button.setBackgroundColor(getResources().getColor(R.color.white));
		}
	}

	static {
		COLUMNS_BY_ROW.put(1, new int[]{1});
		COLUMNS_BY_ROW.put(2, new int[]{1, 1});
		COLUMNS_BY_ROW.put(3, new int[]{1, 1, 1});
		COLUMNS_BY_ROW.put(4, new int[]{2, 2});
		COLUMNS_BY_ROW.put(5, new int[]{2, 3});
		COLUMNS_BY_ROW.put(6, new int[]{3, 3});
		COLUMNS_BY_ROW.put(7, new int[]{1, 3, 3});
		COLUMNS_BY_ROW.put(8, new int[]{2, 3, 3});
		COLUMNS_BY_ROW.put(9, new int[]{3, 3, 3});
		COLUMNS_BY_ROW.put(10, new int[]{2, 2, 3, 3});
		COLUMNS_BY_ROW.put(11, new int[]{2, 3, 3, 3});
		COLUMNS_BY_ROW.put(12, new int[]{3, 3, 3, 3});
	}

	public interface OnButtonsListener {
		/**
		 * 
		 * @param key the button associated key
		 * @param isCorrect <code>true</code> if the key corresponds to a right answer, 
		 * 	<code>false</code> if not
		 * @param isMarked <code>true</code> if the user already tried answering the current question,
		 * 	<code>false</code> if not. 
		 * 	It can be used to let the user play all the tracks associated with the quiz
		 */
		public void onQuizButtonClick(int key, boolean isCorrect, boolean isMarked);

		public ArrayList<QuizButton> onStart();
		
		public void onButtonsViewCreated();
	}
}

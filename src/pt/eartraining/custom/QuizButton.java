package pt.eartraining.custom;

public class QuizButton {
	private int mKey;
	private String mLabel;
	
	public QuizButton(int key, String label) {
		mKey = key;
		mLabel = label;
	}
	
	public int getKey() {
		return mKey;
	}
	
	public String getLabel() {
		return mLabel;
	}
}

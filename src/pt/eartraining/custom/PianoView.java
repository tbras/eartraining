package pt.eartraining.custom;

import pt.eartraining.app.R;
import pt.eartraining.custom.PianoKeyboard.NoteLabel;
import pt.eartraining.custom.PianoKeyboard.PianoKeyboardListener;
import pt.eartraining.custom.PianoKeyboard.ScrollType;

import pt.eartraining.musictheory.NotesUtils;
import pt.eartraining.soundengine.MusicPlayer;
import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class PianoView extends LinearLayout implements PianoKeyboardListener {
	public static final int WHITE_KEYS_COUNT = 52;
	public static final int BLACK_KEYS_COUNT = 36;

	private boolean mPlayable = false;
	private PianoKeyboardListener mListener;
	private PianoKeyboard mKeyboard;

	public PianoView(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater inflater = LayoutInflater.from(context.getApplicationContext());
		inflater.inflate(R.layout.piano_view, this);

		mKeyboard = (PianoKeyboard) findViewById(R.id.keyboard);
		mKeyboard.setPianoKeyboardListener(this);

		Log.i("Context", context.toString());

		setClickable(true);
	}

	public void setPianoKeyboardListener(PianoKeyboardListener listener) {
		mListener = listener;
	}
	
	public void scrollToNote(int note) { 
		mKeyboard.scrollToNote(note, ScrollType.CENTER, true);
	}
	
	// Note labels
	public void addNoteLabel(NoteLabel label) { mKeyboard.addNoteLabel(label); }
	public void addNoteLabels(NoteLabel[] labels) { mKeyboard.addNoteLabels(labels); }
	public void clearNoteLabels() { mKeyboard.clearNoteLabels(); }
	public void undoNoteLabel() { mKeyboard.removeLastNoteLabel(); }
	public void changeNoteLabelsPaint(Paint paint) { mKeyboard.changeNoteLabelsPaint(paint); }
	
	// Key colors
	public void setKeyPaint(int note, Paint paint) { mKeyboard.setKeyPaint(note, paint); }
	public void setKeyPaints(int[] notes, Paint paint) { mKeyboard.setKeyPaints(notes, paint); }
	public void clearKeyPaints() { mKeyboard.clearKeyPaints(); }

	public boolean isPlayable() { return mPlayable; }
	public void setPlayable(boolean playable) { mPlayable = playable; }
//	public boolean isMarkKeys() { return mMarkKeys; }
//	public void setMarkKeys(boolean markKeys) { mMarkKeys = markKeys; }
	
//	public void addSelectedNote(int note) {
//		mSelectedNotes.add(note);
//		
//		mKeyboard.addNoteLabel(new NoteLabel(note, String.valueOf(mSelectedNotes.size()), KeyboardPaint.SELECTED_KEY));
//	}
//	
//	public void undoSelect() {
//		if (mSelectedNotes.size() > 0) {
//			mSelectedNotes.remove(mSelectedNotes.size() - 1);
//		}
//		mKeyboard.removeLastNoteLabel();
//	}
	
	@Override
	public void onNotePress(PianoKeyboard keyboard, int note) {
		Log.i("Piano", "Current: " + NotesUtils.getNoteName(note));

		if (mPlayable) {
			MusicPlayer.getInstance().playNote(note, 3.5);
		}

//		if (mMarkKeys) {
//			mSelectedNotes.add(note);
//			
//			mKeyboard.addNoteLabel(new NoteLabel(note, String.valueOf(mSelectedNotes.size()), KeyboardPaint.SELECTED_KEY));
//		}

		if (mListener != null) {
			mListener.onNotePress(keyboard, note);
		} else {

		}
	}
}

package pt.eartraining.custom;

import java.util.ArrayList;
import java.util.Arrays;

import pt.eartraining.app.App;
import pt.eartraining.app.R;
import pt.eartraining.core.KeyboardPaint;
import pt.eartraining.musictheory.Note;
import pt.eartraining.musictheory.NotesUtils;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

public class PianoKeyboard extends ImageView {
	public static enum ScrollType { LEFT, CENTER };

	private static final int WHITE_KEYS_COUNT = 52;
	private static final float WIDTH_RATIO = 0.22f;
	private static final float RADIUS = 10.0f;
	private static final float RED_BAR_HEIGHT_RATIO = 0.05f;
	private static final float BLACK_KEY_HEIGHT_RATIO = 0.55f;
	private static final float BLACK_KEY_WIDTH_RATIO = 0.58f;
	private static final float[] BLACK_KEY_IN_OCTAVE_POSITION = {
		0.125000f, 0.380952f, 0.541666f, 0.791666f, 0.958333f
	}; 

	// Dark holo colors
	public static final int COLOR_DARK_RED = 0xffcc0000;
	public static final int COLOR_DARK_BLUE = 0xff0099cc; 
	public static final int COLOR_DARK_GREEN = 0xff669900;
	public static final int COLOR_DARK_YELLOW = 0xffff8800;
	public static final int COLOR_DARK_PURPLE = 0xff9933cc;

	// Light holo colors
	public static final int COLOR_LIGHT_RED = 0xffff4444;
	public static final int COLOR_LIGHT_BLUE = 0xff33b5e5; 
	public static final int COLOR_LIGHT_GREEN = 0xff99cc00;
	public static final int COLOR_LIGHT_YELLOW = 0xffffbb33;
	public static final int COLOR_LIGHT_PURPLE = 0xffaa66cc;

	private int[] mBlackKeyOctavePositions = new int[5];
	private int[] mNoteOctavePositions = new int[12];
	private int mOctaveWidth;

	private int mWhiteKeyHeight;
	private int mWhiteKeyWidth;
	private int mBlackKeyHeight;
	private int mBlackKeyWidth;

	private RectF mWhiteKeyRect;
	private RectF mBlackKeyRect;
	private RectF mRedBarRect;

	// Custom attributes
	private boolean mShowNotesNames;

	private PianoKeyboardListener mListener;

	private SparseArray<Paint> mKeyPaints = new SparseArray<Paint>();
	private ArrayList<NoteLabel> mNoteLabels = new ArrayList<NoteLabel>();

	public interface PianoKeyboardListener {
		public void onNotePress(PianoKeyboard keyboard, int note);
	}

	public PianoKeyboard(Context context, AttributeSet attrs) {
		super(context, attrs);

		mWhiteKeyRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
		mBlackKeyRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
		mRedBarRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f); 

		TypedArray t = context.obtainStyledAttributes(attrs, R.styleable.PianoKeyboardAttributes);
		//		mPlayable = t.getBoolean(getResources().getInteger(R.integer.PianoKeyboard_playable_index), false);
		mShowNotesNames = t.getBoolean(getResources().getInteger(R.integer.PianoKeyboard_show_notes_names_index), true);
		t.recycle();

		setClickable(true);
		setOnTouchListener(touchListener);
	}

	public void setPianoKeyboardListener(PianoKeyboardListener listener) {
		mListener = listener;
	}

	public void setShowNotesNames(boolean showNotesNames) {
		mShowNotesNames = showNotesNames;

		invalidate();
	}

	public String getNoteName(int note) {
		return NotesUtils.getNoteName(note);
	}
	
	public void setKeysColors(SparseArray<Paint> colors) {
		mKeyPaints = colors;
	}

	private OnTouchListener touchListener = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {			
			if (event.getAction() == MotionEvent.ACTION_UP) {
				int note = noteFromTouchPosition((int)event.getX(), (int)event.getY());

				// Make sure the note is valid
				if (note > NotesUtils.MIDI_HIGHEST_NOTE) {
					note = NotesUtils.MIDI_HIGHEST_NOTE;	
				}

				if (mListener != null) {
					mListener.onNotePress(PianoKeyboard.this, note);
				}

				return true;
			}

			return false;
		}
	};

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		canvas.translate(getPaddingLeft(), getPaddingTop());

		drawKeyboard(canvas);

		if (mShowNotesNames) {
			drawNotesNames(canvas);
			drawNoteLabels(canvas);
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int height = MeasureSpec.getSize(heightMeasureSpec);
		mWhiteKeyHeight = height - getPaddingTop() - getPaddingBottom();
		mWhiteKeyWidth = (int)((float)height * WIDTH_RATIO);
		int width = (mWhiteKeyWidth * WHITE_KEYS_COUNT) + getPaddingLeft() + getPaddingRight();

		mBlackKeyHeight = (int)((float)mWhiteKeyHeight * BLACK_KEY_HEIGHT_RATIO);
		mBlackKeyWidth = (int)((float)mWhiteKeyWidth * BLACK_KEY_WIDTH_RATIO);

		mWhiteKeyRect.right = mWhiteKeyWidth;
		mWhiteKeyRect.bottom = mWhiteKeyHeight;
		mBlackKeyRect.right = mBlackKeyWidth;
		mBlackKeyRect.bottom = mBlackKeyHeight;

		mRedBarRect.left = -3.0f;
		mRedBarRect.right = width - getPaddingLeft() - getPaddingRight() + 6.0f;
		mRedBarRect.bottom = (int)((float)mWhiteKeyHeight * RED_BAR_HEIGHT_RATIO);
		mRedBarRect.top = -3.0f;

		mOctaveWidth = mWhiteKeyWidth * 7;

		for (int i = 0; i < mBlackKeyOctavePositions.length; i++) {
			mBlackKeyOctavePositions[i] = (int)((float)mOctaveWidth * BLACK_KEY_IN_OCTAVE_POSITION[i]);
		}

		mNoteOctavePositions[0] = 0;							// A
		mNoteOctavePositions[1] = mBlackKeyOctavePositions[0];	// A#
		mNoteOctavePositions[2] = mWhiteKeyWidth;				// B
		mNoteOctavePositions[3] = mWhiteKeyWidth * 2;			// C
		mNoteOctavePositions[4] = mBlackKeyOctavePositions[1];	// C#
		mNoteOctavePositions[5] = mWhiteKeyWidth * 3;			// D
		mNoteOctavePositions[6] = mBlackKeyOctavePositions[2];	// D#
		mNoteOctavePositions[7] = mWhiteKeyWidth * 4;			// E
		mNoteOctavePositions[8] = mWhiteKeyWidth * 5;			// F
		mNoteOctavePositions[9] = mBlackKeyOctavePositions[3];	// F#
		mNoteOctavePositions[10] = mWhiteKeyWidth * 6;			// G
		mNoteOctavePositions[11] = mBlackKeyOctavePositions[4];	// G#

		scrollToNote(62);	// Middle D

		setMeasuredDimension(width, height);
	}

	private void drawKeyboard(Canvas canvas) {
		final int[] whiteKeyFormula = {0, 2, 3, 5, 7, 8, 10};
		final int[] blackKeyFormula = {0, 3, 5, 8, 10};

		// Draw white keys
		canvas.save();
		for (int i = 0; i < WHITE_KEYS_COUNT; i++) {
			// Note for white key number (starting in A [MIDI: 21])
			int note = (i / 7) * 12 + whiteKeyFormula[i % 7] + 21;

			Paint paint = (note == 60) ? KeyboardPaint.MIDDLE_C : KeyboardPaint.WHITE_KEY;
			
			if (mKeyPaints != null) {
				paint = mKeyPaints.get(note, KeyboardPaint.WHITE_KEY);	
			} 
			
			canvas.drawRoundRect(mWhiteKeyRect, RADIUS, RADIUS, paint);
			canvas.drawRoundRect(mWhiteKeyRect, RADIUS, RADIUS, KeyboardPaint.BLACK_BORDER);
			canvas.translate(mWhiteKeyWidth, 0.0f);
		}
		canvas.restore();

		// Draw black keys
		canvas.save();
		int octaveWidth = mWhiteKeyWidth * 7;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 5; j++) {
				// Note for black key number (starting in A# [MIDI: 22])
				int note = (i * 12) + blackKeyFormula[j % 5] + 22;

				// Draw just one black key in 8th octave
				if (i == 7 && j == 1) {
					break;
				}
				
				Paint paint = KeyboardPaint.BLACK_KEY;
				
				if (mKeyPaints != null) {
					paint = mKeyPaints.get(note, KeyboardPaint.BLACK_KEY);
				}	

				canvas.save();
				canvas.translate(mBlackKeyOctavePositions[j], 0.0f);
				canvas.drawRoundRect(mBlackKeyRect, RADIUS / 2, RADIUS / 2, paint);
				canvas.drawRoundRect(mBlackKeyRect, RADIUS / 2, RADIUS / 2, KeyboardPaint.BLACK_BORDER);
				canvas.restore();
			}

			canvas.translate(octaveWidth, 0.0f);
		}
		canvas.restore();

		// Draw red bar
		canvas.drawRect(mRedBarRect, KeyboardPaint.RED_STRIPE);
	}

	private void drawNoteLabels(Canvas canvas) {
		Rect bounds = new Rect();
		KeyboardPaint.NOTE_TEXT.getTextBounds("1", 0, 1, bounds);
		float textSize = bounds.top - bounds.bottom;
		
		// Draw numbers
		canvas.save();
		for (NoteLabel label : mNoteLabels) {
			canvas.save();
			Point p = keyPointForNote(label.note, KeyPointAlignment.BOTTOM_CENTER);
			
			float radius = (isBlackKey(label.note) ? mBlackKeyWidth : mWhiteKeyWidth) * 0.4f;
			float x = p.x - getPaddingLeft();
			float y = p.y - (radius * 1.2f) - getPaddingTop();
			
			canvas.translate(0.0f, -radius * 2.1f * label.tier);
			
			canvas.drawCircle(x, y, radius, label.paint);
			canvas.drawText(label.text, x, y - (textSize / 2), KeyboardPaint.NOTE_WHITE_TEXT);
			canvas.restore();
		}
		canvas.restore();
	}
	
	

	private void drawNotesNames(Canvas canvas) {
		int halfWhiteKeyWidth = mWhiteKeyWidth / 2;

		String[] names = null;

		// Hack to avoid error on xml layout view drawing
		if (App.getContext() != null) {
			names = new String[] {
					Note.LA.getName(),
					Note.SI.getName(),
					Note.DO.getName(),
					Note.RE.getName(),
					Note.MI.getName(),
					Note.FA.getName(),
					Note.SOL.getName()
			};
		} else {
			names = new String[] {"A", "B", "C", "D", "E", "F", "G"};
		}

		canvas.save();
		for (int i = 0, j = 0; i < WHITE_KEYS_COUNT; i++) {
			// First two key (A and B) are one octave lower
			int octave = (j <= 1) ? (i / 7) : (i / 7) + 1;

			canvas.drawText(names[j]/* + octave*/, halfWhiteKeyWidth, mWhiteKeyHeight - 15, KeyboardPaint.NOTE_TEXT);
			canvas.translate(mWhiteKeyWidth, 0.0f);

			j = (j == 6) ? 0 : j + 1;
		}
		canvas.restore();
	}

	private int noteFromTouchPosition(int x, int y) {
		//		x += getPaddingLeft();
		//		y += getPaddingTop();

		// Octaves in relation to A Minor 
		int octave = x / mOctaveWidth;
		int xInOctave = x - (mOctaveWidth * octave);

		// Check if a black key was touched
		if (y <= mBlackKeyHeight) {
			// Especial case
			if (octave > 0 && (xInOctave <= mBlackKeyOctavePositions[4] + mBlackKeyWidth - mOctaveWidth)) {
				return calculateNote(octave, -1);
			} else {
				final int[] blackKeySemitones = {1, 4, 6, 9, 11};

				for (int i = 0; i < 5; i++) {
					if (xInOctave >= mBlackKeyOctavePositions[i] && 
							xInOctave <= mBlackKeyOctavePositions[i] + mBlackKeyWidth) {
						return calculateNote(octave, blackKeySemitones[i]);
					}
				}
			}
		}

		final int[] blackKeySemitones = new int[]{0, 2, 3, 5, 7, 8, 10};

		return calculateNote(octave, blackKeySemitones[(xInOctave / mWhiteKeyWidth)]);
	}

	private int calculateNote(int octave, int semioneInOctave) {
		return 21 + (octave * 12) + semioneInOctave;
	}

	public void scrollToNote(int note) { scrollToNote(note, ScrollType.CENTER, true); }
	public void scrollToNote(int note, ScrollType type) { scrollToNote(note, type, true); }
	public void scrollToNote(int note, ScrollType type, boolean smoothScroll) {
		HorizontalScrollView scrollView = (HorizontalScrollView) getParent();

		int x = 0;

		if (type == ScrollType.LEFT) {
			x = keyPositionForNote(note);
		} else {
			x = keyPositionForNote(note) - (scrollView.getWidth() / 2) + (mWhiteKeyWidth / 2);
		}

		if (smoothScroll) {
			scrollView.smoothScrollTo(x, 0);
		} else {
			scrollView.scrollTo(x, 0);
		}
	}

	private int keyPositionForNote(int note) {
		note -= 21;
		int octave = note / 12;
		int noteInOctave = note - (octave * 12);

		return getPaddingLeft() + (octave * mOctaveWidth) + mNoteOctavePositions[noteInOctave];
	}

	public boolean isBlackKey(int note) {
		note -= 21;
		final int octave = note / 12;
		final int noteInOctave = note - (octave * 12);
		final int[] blackKeys = {1, 4, 6, 9, 11};

		if (Arrays.binarySearch(blackKeys, noteInOctave) < 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public enum KeyPointAlignment { BOTTOM_LEFT, BOTTOM_CENTER };
	
	public Point keyPointForNote(int note, KeyPointAlignment alignment) {
		assert(note >= 21);
		
		int octave = (note - 21) / 12;
		int noteInOctave = (note - 21) - (octave * 12);
		boolean isBlack = isBlackKey(note);
		
		// LEFT BOTTOM
		int x = getPaddingLeft() + (octave * mOctaveWidth) + mNoteOctavePositions[noteInOctave];
		int y = getPaddingTop() + (isBlack ? mBlackKeyHeight : mWhiteKeyHeight);

		if (alignment == KeyPointAlignment.BOTTOM_CENTER) {
			x += (isBlack ? mBlackKeyWidth : mWhiteKeyWidth) / 2;
		}
		
		return new Point(x, y);
	}
	
	public Point keyPointForNote(int note) {
		return keyPointForNote(note, KeyPointAlignment.BOTTOM_LEFT);
	}

	public static float convertPixelsToDp(float px,Context context){
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float dp = px / (metrics.densityDpi / 160f);
		return dp;
	}

	public static float convertDpToPixel(float dp,Context context){
		DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		float px = dp * (metrics.densityDpi/160f);
		return px;
	}
	
	// ================================================
	// NOTE LABELS METHODS
	// ================================================
	public void addNoteLabel(NoteLabel label) {
		label.tier = 0;
		
		for (NoteLabel n : mNoteLabels) {
			if (n.note == label.note) {
				label.tier++;
			}
		}
		
		mNoteLabels.add(label);
		
		invalidate();
	}
	
	public void addNoteLabels(NoteLabel[] notes) {
		for (NoteLabel label : notes) {
			label.tier = 0;
			
			for (NoteLabel n : mNoteLabels) {
				if (n.note == label.note) {
					label.tier++;
				}
			}
			
			mNoteLabels.add(label);
		}
		

		invalidate();
	}
	
	public void removeLastNoteLabel() {
		if (mNoteLabels.size() > 0) {
			mNoteLabels.remove(mNoteLabels.size() - 1);
		}
		
		invalidate();
	}
	
	public void clearNoteLabels() {
		mNoteLabels.clear();
		
		invalidate();
	}
	
	public void changeNoteLabelsPaint(Paint paint) {
		for (NoteLabel label : mNoteLabels) {
			label.paint = paint;
		}
		
		invalidate();
	}
	
	// ================================================
	// KEY PAINTS METHODS
	// ================================================
	public void setKeyPaint(int note, Paint paint) {
		mKeyPaints.append(note, paint);
		
		invalidate();
	}
	
	public void setKeyPaints(int[] notes, Paint paint) {
		for (int note : notes) {
			mKeyPaints.append(note, paint);
		}
		
		invalidate();
	}
	
	public void clearKeyPaints() {
		mKeyPaints.clear();
		
		invalidate();
	}

	// ================================================
	// STATIC CLASS - NoteLabel
	// ================================================
	public static class NoteLabel {
		public int note;
		public String text;
		public Paint paint;
		public int tier;

		public NoteLabel(int note, String text, Paint paint) {
			this.note = note;
			this.text = text;
			this.paint = paint;
			this.tier = 0;
		}
	}
}

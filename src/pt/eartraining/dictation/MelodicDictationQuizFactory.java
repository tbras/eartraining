package pt.eartraining.dictation;

import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;
import pt.eartraining.app.JBUtils;
import pt.eartraining.core.ButtonsQuiz;
import pt.eartraining.core.KeyboardPaint;
import pt.eartraining.core.KeyboardQuiz;
import pt.eartraining.core.QuizFactory;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.intervals.IntervalsIdentificationQuizFactory;
import pt.eartraining.musictheory.Scale;
import pt.eartraining.soundengine.MusicTrackFactory;

public class MelodicDictationQuizFactory extends QuizFactory {
	MusicTrackFactory mTrackFactory = new MusicTrackFactory();

	public MelodicDictationQuizFactory(QuizSpecification quizSpecification) {
		super(quizSpecification);
	}

	@Override
	public ButtonsQuiz nextButtonQuiz() {
		return null;
	}

	@Override
	public KeyboardQuiz nextKeyboardQuiz() {
		MelodicDictationSpecification spec = (MelodicDictationSpecification) getQuizSpec();
		Scale scale = spec.getScale();

		int root = getRandomRootNote(spec.getNotesSpan());

		int[] answer = new int[spec.getNotesCount()];

		int[] dictationFormula = new int[spec.getNotesCount()];
		dictationFormula[0] = JBUtils.randInt(0, scale.getFormula().length - 1);

		for (int i = 1; i < dictationFormula.length; i++) {
			int direction = JBUtils.randInt(0, 1) == 0 ? 1 : -1;

			dictationFormula[i] = dictationFormula[i-1] + JBUtils.randInt(0, spec.getMaxJump()) * direction;

			// Clamp values
			if (dictationFormula[i] > spec.getNotesSpan()) {
				dictationFormula[i] = spec.getNotesSpan();
			}

			if (dictationFormula[i] < 0) {
				dictationFormula[i] = 0;
			}
		}

		int[] scaleFormula = scale.getFormula();
		for (int i = 0; i < answer.length; i++) {
			int octave = dictationFormula[i] / scaleFormula.length;
			int numeral =  dictationFormula[i] % scaleFormula.length;

			answer[i] = root +  scaleFormula[numeral] + (octave * 12);
		}

		return new KeyboardQuiz(mTrackFactory.newMelodicDictationTrack(answer), answer);
	}
	
	@Override
	public Paint specialKeysPaint() {
		return KeyboardPaint.SPECIAL_KEY;
	}
	
	@Override
	public int[] specialKeys() {
		return super.specialKeys();
	}

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected MelodicDictationQuizFactory(Parcel in) {
		super(in);

		mTrackFactory = new MusicTrackFactory();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
	}

	public static final Parcelable.Creator<MelodicDictationQuizFactory> CREATOR = new Parcelable.Creator<MelodicDictationQuizFactory>() {
		@Override
		public MelodicDictationQuizFactory createFromParcel(Parcel in) {
			return new MelodicDictationQuizFactory(in);
		}

		@Override
		public MelodicDictationQuizFactory[] newArray(int size) {
			return new MelodicDictationQuizFactory[size];
		}
	};
}

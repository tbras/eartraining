package pt.eartraining.dictation;

import android.os.Parcel;
import android.os.Parcelable;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.musictheory.Scale;

public class MelodicDictationSpecification extends QuizSpecification {
	private int mNotesCount;
	private int mNotesSpan;		// Span scale degrees, 8 = 1-8va for a diatonic scale, 10 = 2-8va for a pentatonic
	private int mMaxJump;	 	// Max jump from one note to another in scale degrees
	private Scale mScale;
	private boolean mFixedDynamics;
	private boolean mFixedRhythm;

	public MelodicDictationSpecification() {
		super();
	}

	public int getNotesCount() { return mNotesCount; }
	public void setNotesCount(int notesCount) { mNotesCount = notesCount; }
	public int getNotesSpan() { return mNotesSpan; }
	public void setNotesSpan(int notesSpan) { mNotesSpan = notesSpan; }
	public int getMaxJump() { return mMaxJump; }
	public void setMaxJump(int maxJump) { mMaxJump = maxJump; }
	public Scale getScale() { return mScale; }
	public void setScale(Scale scale) { mScale = scale; }
	public boolean getFixedDynamics() { return mFixedDynamics; }
	public void setFixedDynamics(boolean fixedDynamics) { mFixedDynamics = fixedDynamics; }
	public boolean getFixedRhythm() { return mFixedRhythm; }
	public void setFixedRhythm(boolean fixedRhythm) { mFixedRhythm = fixedRhythm; }

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected MelodicDictationSpecification(Parcel in) {
		super(in);
		
		mNotesCount = in.readInt();
		mNotesSpan = in.readInt();
		mMaxJump = in.readInt();
		mScale = Scale.fromInt(in.readInt());
		mFixedDynamics = (in.readInt() == 0) ? false : true;
		mFixedRhythm = (in.readInt() == 0) ? false : true;;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		
		dest.writeInt(mNotesCount);
		dest.writeInt(mNotesSpan);
		dest.writeInt(mMaxJump);
		dest.writeInt(mScale.ordinal());
		dest.writeInt(mFixedDynamics ? 1 : 0);
		dest.writeInt(mFixedRhythm ? 1 : 0);
	}

	public static final Parcelable.Creator<MelodicDictationSpecification> CREATOR = new Parcelable.Creator<MelodicDictationSpecification>() {
		@Override
		public MelodicDictationSpecification createFromParcel(Parcel in) {
			return new MelodicDictationSpecification(in);
		}

		@Override
		public MelodicDictationSpecification[] newArray(int size) {
			return new MelodicDictationSpecification[size];
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (mFixedDynamics ? 1231 : 1237);
		result = prime * result + (mFixedRhythm ? 1231 : 1237);
		result = prime * result + mMaxJump;
		result = prime * result + mNotesCount;
		result = prime * result + mNotesSpan;
		result = prime * result + ((mScale == null) ? 0 : mScale.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MelodicDictationSpecification other = (MelodicDictationSpecification) obj;
		if (mFixedDynamics != other.mFixedDynamics)
			return false;
		if (mFixedRhythm != other.mFixedRhythm)
			return false;
		if (mMaxJump != other.mMaxJump)
			return false;
		if (mNotesCount != other.mNotesCount)
			return false;
		if (mNotesSpan != other.mNotesSpan)
			return false;
		if (mScale != other.mScale)
			return false;
		return true;
	}
}

package pt.eartraining.dictation;

import android.content.Intent;
import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.KeyboardQuizActivity;
import pt.eartraining.core.QuizFactory;
import pt.eartraining.core.QuizLobbyActivity;
import pt.eartraining.core.SpecificationDialogFragment;
import pt.eartraining.core.SpecificationsDataSource;

public class MelodicDictationLobby extends QuizLobbyActivity {

	@Override
	protected SpecificationDialogFragment getSpecDialogFragment() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected SpecificationsDataSource getSpecsDataSource() {
		return new MelodicDictationDataSource();
	}

	@Override
	protected Class<?> getQuizFactoryClass() {
		return MelodicDictationQuizFactory.class;
	}

	@Override
	protected EarTrainingSubject getSubject() {
		return EarTrainingSubject.MelodicDictation;
	}
	
	@Override
	public void onListItemSelected(String tag, int position) {		
		QuizFactory factory = QuizFactory.newInstance(getQuizFactoryClass(), getQuizSpecification(tag, position));
		Intent intent = KeyboardQuizActivity.newIntent(this, factory);

		startActivityForResult(intent, QUIZ_REQUEST_CODE);
	}	
}

package pt.eartraining.dictation;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import pt.eartraining.app.R;
import pt.eartraining.core.DatabaseHelper.Column;
import pt.eartraining.core.EarTrainingSubject;
import pt.eartraining.core.QuizSpecification;
import pt.eartraining.core.SpecificationsDataSource;
import pt.eartraining.musictheory.Chord;
import pt.eartraining.musictheory.Scale;

public class MelodicDictationDataSource extends SpecificationsDataSource {
	private static final String COLUMN_NOTES_COUNT = "notes_count";
	private static final String COLUMN_NOTES_SPAN = "notes_span";
	private static final String COLUMN_MAX_JUMP = "max_jump";
	private static final String COLUMN_SCALE = "scale";
	private static final String COLUMN_FIXED_DYNAMICS = "fixed_dynamics";
	private static final String COLUMN_FIXED_RHYTHM = "fixed_rhythm";
	private final String[] COLUMN_NAMES = { COLUMN_ID, COLUMN_SUBJECT,
			COLUMN_TITLE, COLUMN_SUBTITLE, COLUMN_VALIDATION_COUNT, COLUMN_NOTES_COUNT, 
			COLUMN_NOTES_SPAN, COLUMN_MAX_JUMP, COLUMN_SCALE, COLUMN_FIXED_DYNAMICS, COLUMN_FIXED_RHYTHM };
	
	@Override
	public ArrayList<QuizSpecification> getSpecifications(SQLiteDatabase db,
			boolean custom) {
		ArrayList<QuizSpecification> specs = new ArrayList<QuizSpecification>();

		Cursor cursor = db.query(dataSourceName(), 
				COLUMN_NAMES,  COLUMN_CUSTOM + " = " + (custom ? 1 : 0), 
				null, null, null, COLUMN_ID);

		while (cursor.moveToNext()) {
			MelodicDictationSpecification spec = new MelodicDictationSpecification();
			spec.setId(cursor.getLong(0));
			spec.setSubject(EarTrainingSubject.fromInt(cursor.getInt(1)));
			spec.setTitle(cursor.getString(2));
			spec.setSubtitle(cursor.getString(3));
			spec.setValidationCount(cursor.getInt(4));
			spec.setNotesCount(cursor.getInt(5));
			spec.setNotesSpan(cursor.getInt(6));
			spec.setMaxJump(cursor.getInt(7));
			spec.setScale(Scale.valueOf(cursor.getString(8)));
			spec.setFixedDynamics(cursor.getInt(9) == 1 ? true : false);
			spec.setFixedRhythm(cursor.getInt(10) == 1 ? true : false);

			specs.add(spec);
		}

		return specs;
	}

	@Override
	public boolean addCustomSpec(SQLiteDatabase db, QuizSpecification spec) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String dataSourceName() {
		return "melodic_dictation";
	}

	@Override
	public Column[] dataSourceColumns() {
		Column[] columns = new Column[] {
				new Column(COLUMN_ID, Column.ColumnType.INTEGER, true),
				new Column(COLUMN_SUBJECT, Column.ColumnType.INTEGER),
				new Column(COLUMN_CUSTOM, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_TITLE, Column.ColumnType.TEXT, false),
				new Column(COLUMN_SUBTITLE, Column.ColumnType.TEXT, false),
				new Column(COLUMN_VALIDATION_COUNT, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_NOTES_COUNT, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_NOTES_SPAN, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_MAX_JUMP, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_SCALE, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_FIXED_DYNAMICS, Column.ColumnType.INTEGER, false),
				new Column(COLUMN_FIXED_RHYTHM, Column.ColumnType.INTEGER, false)
		};

		return columns;
	}

	@Override
	public void loadDefaultData(SQLiteDatabase db) {
		JSONArray specs = loadJSONFile(R.raw.melodic_dictation);

		try {
			db.beginTransaction();

			for (int i = 0; i < specs.length(); i++) {
				JSONObject spec = specs.getJSONObject(i);
				
				boolean fixedRhythm = spec.optBoolean(COLUMN_FIXED_RHYTHM, true);
				
				ContentValues values = new ContentValues();
				values.put(COLUMN_TITLE, spec.optString(COLUMN_TITLE, "Not Set"));
				values.put(COLUMN_SUBTITLE, fixedRhythm ? "Fixed rhythm" : "Variable rhythm");
				values.put(COLUMN_SUBJECT, getSubject().ordinal());
				values.put(COLUMN_CUSTOM, 0);
				values.put(COLUMN_VALIDATION_COUNT, 10);
				values.put(COLUMN_NOTES_COUNT, spec.getInt(COLUMN_NOTES_COUNT));
				values.put(COLUMN_NOTES_SPAN, spec.optInt(COLUMN_NOTES_SPAN, 8));
				values.put(COLUMN_MAX_JUMP, spec.optInt(COLUMN_MAX_JUMP, 8));
				values.put(COLUMN_SCALE, spec.optString(COLUMN_SCALE, Scale.MAJOR.toString()));
				values.put(COLUMN_FIXED_DYNAMICS, spec.optBoolean(COLUMN_FIXED_DYNAMICS, true) ? 1 : 0);
				values.put(COLUMN_FIXED_RHYTHM, fixedRhythm ? 1 : 0);

				// Insert row into database
				long id = db.insert(dataSourceName(), null, values);

				if (id == -1) {
					Log.e("Error", "Could not insert '" + values.toString() + "'");
				} else {
					Log.i("DBInfo", "Inserted interval with id = " + id);
				}
			}

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			db.endTransaction();
		}
	}

	@Override
	public EarTrainingSubject getSubject() {
		return EarTrainingSubject.MelodicDictation;
	}
}

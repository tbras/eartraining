package pt.eartraining.core;

import android.database.sqlite.SQLiteDatabase;
import pt.eartraining.core.DatabaseHelper.Column;

public abstract class DataSource {
	public abstract String dataSourceName();
	public abstract Column[] dataSourceColumns();
	public abstract void loadDefaultData(SQLiteDatabase db);
}

package pt.eartraining.core;

import android.graphics.Paint;
import android.graphics.Paint.Align;

public class KeyboardPaint {
	public static final Paint BLACK_BORDER;
	public static final Paint WHITE_KEY;
	public static final Paint BLACK_KEY;
	public static final Paint RED_STRIPE;
	public static final Paint MIDDLE_C;
	public static final Paint NOTE_TEXT;
	public static final Paint SELECTED_KEY;
	public static final Paint RIGHT_KEY;
	public static final Paint WRONG_KEY;
	public static final Paint NOTE_WHITE_TEXT;
	public static final Paint SPECIAL_KEY;
	
	static {
		BLACK_BORDER = new Paint(Paint.ANTI_ALIAS_FLAG);
		BLACK_BORDER.setStyle(Paint.Style.STROKE);
		BLACK_BORDER.setStrokeWidth(1.0f);
		BLACK_BORDER.setStrokeCap(Paint.Cap.ROUND);
		BLACK_BORDER.setColor(0xff000000);
		
		WHITE_KEY = new Paint(Paint.ANTI_ALIAS_FLAG);
		WHITE_KEY.setStyle(Paint.Style.FILL);
		WHITE_KEY.setStrokeWidth(1.0f);
		WHITE_KEY.setStrokeCap(Paint.Cap.ROUND);
		WHITE_KEY.setColor(0xffffffff);
		
		BLACK_KEY = new Paint(Paint.ANTI_ALIAS_FLAG);
		BLACK_KEY.setStyle(Paint.Style.FILL);
		BLACK_KEY.setStrokeWidth(1.0f);
		BLACK_KEY.setStrokeCap(Paint.Cap.ROUND);
		BLACK_KEY.setColor(0xff000000);
		
		SELECTED_KEY = new Paint(Paint.ANTI_ALIAS_FLAG);
		SELECTED_KEY.setStyle(Paint.Style.FILL_AND_STROKE);
		SELECTED_KEY.setStrokeWidth(1.0f);
		SELECTED_KEY.setStrokeCap(Paint.Cap.ROUND);
		SELECTED_KEY.setColor(0xff44c6f6);
		
		RIGHT_KEY = new Paint(Paint.ANTI_ALIAS_FLAG);
		RIGHT_KEY.setStyle(Paint.Style.FILL);
		RIGHT_KEY.setStrokeWidth(1.0f);
		RIGHT_KEY.setStrokeCap(Paint.Cap.ROUND);
		RIGHT_KEY.setColor(0xff99cc00);
		
		WRONG_KEY = new Paint(Paint.ANTI_ALIAS_FLAG);
		WRONG_KEY.setStyle(Paint.Style.FILL_AND_STROKE);
		WRONG_KEY.setStrokeWidth(1.0f);
		WRONG_KEY.setStrokeCap(Paint.Cap.ROUND);
		WRONG_KEY.setColor(0xffff4444);
		
		SPECIAL_KEY = new Paint(Paint.ANTI_ALIAS_FLAG);
		SPECIAL_KEY.setStyle(Paint.Style.FILL);
		SPECIAL_KEY.setStrokeWidth(1.0f);
		SPECIAL_KEY.setStrokeCap(Paint.Cap.ROUND);
		SPECIAL_KEY.setColor(0xff55d7f7);
		
		RED_STRIPE = new Paint(Paint.ANTI_ALIAS_FLAG);
		RED_STRIPE.setStyle(Paint.Style.FILL);
		RED_STRIPE.setColor(0xfffb0005);
		
		MIDDLE_C = new Paint(Paint.ANTI_ALIAS_FLAG);
		MIDDLE_C.setStyle(Paint.Style.FILL);
		MIDDLE_C.setColor(0xffdddddd);
		
		NOTE_TEXT = new Paint(Paint.ANTI_ALIAS_FLAG);
		NOTE_TEXT.setTextSize(30.0f);
		NOTE_TEXT.setTextAlign(Align.CENTER);
		NOTE_TEXT.setColor(0xff666666);
		
		NOTE_WHITE_TEXT = new Paint(Paint.ANTI_ALIAS_FLAG);
		NOTE_WHITE_TEXT.setTextSize(30.0f);
		NOTE_WHITE_TEXT.setTextAlign(Align.CENTER);
		NOTE_WHITE_TEXT.setColor(0xffffffff);
	}
}

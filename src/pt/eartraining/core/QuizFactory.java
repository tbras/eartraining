package pt.eartraining.core;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;

import pt.eartraining.app.AppPreferences;
import pt.eartraining.app.JBUtils;
import pt.eartraining.musictheory.NotesUtils;

public abstract class QuizFactory implements Parcelable {
	protected QuizSpecification mQuizSpec;
	private int mLowestNote;
	private int mHighestNote;
	private int mTempoInBPM;

	protected QuizFactory(QuizSpecification quizSpecification) {
		mQuizSpec = quizSpecification;
		mLowestNote = AppPreferences.getInstance().getLowestNote();
		mHighestNote = AppPreferences.getInstance().getHighestNote();
		mTempoInBPM = AppPreferences.getInstance().getTempoInBPM();
	}

	public QuizSpecification getQuizSpec() { return mQuizSpec; }
	//	public void setQuizSpec(QuizSpecification quizSpec) { this.mQuizSpec = quizSpec; }
	public int getLowestNote() { return mLowestNote; }
	public void setLowestNote(int lowestNote) { this.mLowestNote = lowestNote; }
	public int getHighestNote() { return mHighestNote; }
	public void setHighestNote(int highestNote) { this.mHighestNote = highestNote; }
	public int getTempoInBPM() { return mTempoInBPM; }

	public static QuizFactory newInstance(Class<?> quizFactoryClass, QuizSpecification quizSpecification) {
		QuizFactory factory = null;

		try {
			Constructor<?> c = (Constructor<?>) quizFactoryClass.getConstructor(QuizSpecification.class);
			factory = (QuizFactory) c.newInstance(quizSpecification);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return factory;
	}

	protected int getRandomRootNote(int minNoteMaxNoteSpan) {
		int first = getLowestNote();
		int last = getHighestNote();

		// Normalize first and last so all notes are audible (A1 to C8) 
		first = first - minNoteMaxNoteSpan < NotesUtils.MIDI_LOWEST_NOTE ? NotesUtils.MIDI_LOWEST_NOTE + minNoteMaxNoteSpan : first;
		last = last + minNoteMaxNoteSpan > NotesUtils.MIDI_HIGHEST_NOTE ? NotesUtils.MIDI_HIGHEST_NOTE - minNoteMaxNoteSpan : last;

		return JBUtils.randInt(first, last);
	}
	
	public Paint specialKeysPaint() {
		return null;
	}
	
	public int[] specialKeys() {
		return null;
	}

	// ==================================================
	// ABSTRACT METHODS
	// ==================================================
	public abstract ButtonsQuiz nextButtonQuiz();
	public abstract KeyboardQuiz nextKeyboardQuiz();

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected QuizFactory(Parcel in) {
		mQuizSpec = in.readParcelable(QuizSpecification.class.getClassLoader());
		mLowestNote = in.readInt();
		mHighestNote = in.readInt();
		mTempoInBPM = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(mQuizSpec, flags);
		dest.writeInt(mLowestNote);
		dest.writeInt(mHighestNote);
		dest.writeInt(mTempoInBPM);
	}

	public static final Parcelable.Creator<QuizFactory> CREATOR = new Parcelable.Creator<QuizFactory>() {
		@Override
		public QuizFactory createFromParcel(Parcel source) {
			return null;
		}

		@Override
		public QuizFactory[] newArray(int size) {
			return null;
		}
	};

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mHighestNote;
		result = prime * result + mLowestNote;
		result = prime * result
				+ ((mQuizSpec == null) ? 0 : mQuizSpec.hashCode());
		result = prime * result + mTempoInBPM;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuizFactory other = (QuizFactory) obj;
		if (mHighestNote != other.mHighestNote)
			return false;
		if (mLowestNote != other.mLowestNote)
			return false;
		if (mQuizSpec == null) {
			if (other.mQuizSpec != null)
				return false;
		} else if (!mQuizSpec.equals(other.mQuizSpec))
			return false;
		if (mTempoInBPM != other.mTempoInBPM)
			return false;
		return true;
	}
}

package pt.eartraining.core;

import android.os.Parcel;
import android.os.Parcelable;

public abstract class QuizSpecification implements Parcelable {
	public static final String EXTRA_ID = "id";

	// Private fields
	private long mId;
	private EarTrainingSubject mSubject;
	private String mTitle;
	private String mSubtitle;
	private int mValidationCount;

	public QuizSpecification() {}

	@Override
	public String toString() {
		return "QuizSpecification [id=" + mId + ", subject=" + mSubject
				+ ", title=" + mTitle + ", subtitle=" + mSubtitle
				+ ", validationCount=" + mValidationCount + "]";
	}

	// Getters and setters
	public long getId() { return mId; }
	public void setId(long id) { this.mId = id; }
	public EarTrainingSubject getSubject() { return mSubject; }
	public void setSubject(EarTrainingSubject subject) { this.mSubject = subject; }
	public String getTitle() { return mTitle; }
	public void setTitle(String title) { this.mTitle = title; }
	public String getSubtitle() { return mSubtitle; }
	public void setSubtitle(String subtitle) { this.mSubtitle = subtitle; }
	public int getValidationCount() { return mValidationCount; }
	public void setValidationCount(int validationCount) { this.mValidationCount = validationCount; }

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected QuizSpecification(Parcel in) {
		mId = in.readLong();
		mSubject = EarTrainingSubject.fromInt(in.readInt());
		mTitle = in.readString();
		mSubtitle = in.readString();
		mValidationCount = in.readInt();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(mId);
		dest.writeInt(mSubject.ordinal());
		dest.writeString(mTitle);
		dest.writeString(mSubtitle);
		dest.writeInt(mValidationCount);
	}

	public static final Parcelable.Creator<QuizSpecification> CREATOR = new Parcelable.Creator<QuizSpecification>() {
		@Override
		public QuizSpecification createFromParcel(Parcel source) {
			return null;
		}

		@Override
		public QuizSpecification[] newArray(int size) {
			return null;
		}
	};
	
	@Override
	public int describeContents() {
		return 0;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (mId ^ (mId >>> 32));
		result = prime * result
				+ ((mSubject == null) ? 0 : mSubject.hashCode());
		result = prime * result
				+ ((mSubtitle == null) ? 0 : mSubtitle.hashCode());
		result = prime * result + ((mTitle == null) ? 0 : mTitle.hashCode());
		result = prime * result + mValidationCount;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuizSpecification other = (QuizSpecification) obj;
		if (mId != other.mId)
			return false;
		if (mSubject != other.mSubject)
			return false;
		if (mSubtitle == null) {
			if (other.mSubtitle != null)
				return false;
		} else if (!mSubtitle.equals(other.mSubtitle))
			return false;
		if (mTitle == null) {
			if (other.mTitle != null)
				return false;
		} else if (!mTitle.equals(other.mTitle))
			return false;
		if (mValidationCount != other.mValidationCount)
			return false;
		return true;
	}
}

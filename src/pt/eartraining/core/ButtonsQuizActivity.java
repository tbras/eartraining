package pt.eartraining.core;

import pt.eartraining.app.R;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import pt.eartraining.core.QuizButtonsView.QuizButtonsListener;
import pt.eartraining.custom.PianoView;
import pt.eartraining.soundengine.MusicPlayer;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class ButtonsQuizActivity extends SherlockFragmentActivity {	
	private static final String EXTRA_CLASS_QUIZ_FACTORY = "factory";

	private ScoreDataSource mScoreManager;
	private QuizFactory mQuizFactory;

	private MusicPlayer mPlayer;

	private QuizButtonsView mButtonsView;
	private PianoView mPiano;
	private TextView mTextViewScore;
	private TextView mTextViewTotalAnswers;
	private TextView mTextViewRatio;
	private ProgressBar mProgressBarToValidation;

	private Button mReplay;
	private Button mNext;

	// Statistics related
	private int mScore = 0;
	private int mQuestionsAnswered = 0;
	private int mRightAnswers = 0;
	private int mValidationCount = 0;
	private int mStreak = 0;

	private ButtonsQuiz mCurrentQuiz;

	public static Intent newIntent(Context ctx, QuizFactory factory) {
		Intent intent = new Intent(ctx, ButtonsQuizActivity.class);

		Bundle extras = new Bundle();
		extras.putParcelable(EXTRA_CLASS_QUIZ_FACTORY, factory);

		intent.putExtras(extras);

		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_quiz);

		mPlayer = MusicPlayer.getInstance();
		mScoreManager = new ScoreDataSource();

		Bundle extras = getIntent().getExtras();
		
		mQuizFactory = extras.getParcelable(EXTRA_CLASS_QUIZ_FACTORY);

		mValidationCount = mQuizFactory.getQuizSpec().getValidationCount();

		mPiano = (PianoView) findViewById(R.id.piano_view);
		mButtonsView = (QuizButtonsView) findViewById(R.id.quiz_buttons_view);
		mButtonsView.setButtonsListener(mQuizButtonsViewListener);
		mTextViewScore = (TextView) findViewById(R.id.tv_score_value);
		mTextViewTotalAnswers = (TextView) findViewById(R.id.tv_total_answers_value);
		mTextViewRatio = (TextView) findViewById(R.id.tv_right_answers_ratio_value);
		mProgressBarToValidation = (ProgressBar) findViewById(R.id.pb_validation);
		mProgressBarToValidation.setMax(mValidationCount);
		mProgressBarToValidation.setProgress(0);

		// Setup bottom buttons
		mReplay = (Button) findViewById(R.id.replay_audio);
		mReplay.setOnClickListener(mReplayOnClickListener);
		mReplay.setEnabled(false);
		mNext = (Button) findViewById(R.id.next_question);
		mNext.setOnClickListener(mNextOnClickListener);
		mNext.setEnabled(false);

		// bah
		mPiano.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 3));
		mButtonsView.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 6));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		mPlayer.stopTrack();
	}

	@Override
	public void onBackPressed() {
		if (mQuestionsAnswered >= mValidationCount) {
			updateScoreDatabase();

			setResult(RESULT_OK);
		} else {
			setResult(RESULT_CANCELED);
		}

		super.onBackPressed();
	}

	private void updateScoreDatabase() {
		QuizSpecification spec = mQuizFactory.getQuizSpec();

		mScoreManager.updateHighScore(spec.getSubject(), spec.getId(), getRightAnswersRatio());
	}

	private OnClickListener mReplayOnClickListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			if (mCurrentQuiz != null && mCurrentQuiz.getQuestion() != null) {
				mPlayer.playTrack(mCurrentQuiz.getQuestion());
			}
		}
	};

	private OnClickListener mNextOnClickListener = new OnClickListener() {	
		@Override
		public void onClick(View v) {
			mNext.setEnabled(false);
			mCurrentQuiz = mQuizFactory.nextButtonQuiz();
			mButtonsView.setupView(mCurrentQuiz);

			mPlayer.playTrack(mCurrentQuiz.getQuestion());
		}
	};

	private QuizButtonsListener mQuizButtonsViewListener = new QuizButtonsListener() {

		@Override
		public ButtonsQuiz onStart() {
			mReplay.setEnabled(true);
			mCurrentQuiz = mQuizFactory.nextButtonQuiz();

			mPlayer.playTrack(mCurrentQuiz.getQuestion());

			return mCurrentQuiz;
		}

		@Override
		public void onButtonClick(ButtonsQuiz quiz, ButtonQuizAnswer answer,
				int attemptsCounter) {
			if (attemptsCounter == 1) {
				mQuestionsAnswered++;

				if (quiz.validateAnswer(answer)) {
					mRightAnswers++;
					mStreak++;
				} else {
					mStreak = 0;
				}

				mScore += mStreak;

				updateScore();
				updateQuestionsAnswered();
				updateProgress();
				updateRatio();

				mNext.setEnabled(true);
			} else {
				mPlayer.playTrack(answer.getTrack());
			}
		}
	};

	private void updateScore() {
		mTextViewScore.setText(String.valueOf(mScore));
	}

	private void updateQuestionsAnswered() {
		mTextViewTotalAnswers.setText(String.valueOf(mQuestionsAnswered));
	}

	private void updateProgress() {
		if (mQuestionsAnswered <= mValidationCount) {
			mProgressBarToValidation.setProgress(mQuestionsAnswered);
		}
	}

	private void updateRatio() {
		mTextViewRatio.setText(String.valueOf(getRightAnswersRatio()));
	}

	private int getRightAnswersRatio() {
		return (mQuestionsAnswered == 0) ? 0 : (mRightAnswers * 100) / mQuestionsAnswered;
	}
}

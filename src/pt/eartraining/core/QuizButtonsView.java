package pt.eartraining.core;

import java.util.ArrayList;

import pt.eartraining.app.R;
import pt.eartraining.core.ButtonsQuiz.ButtonQuizAnswer;
import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class QuizButtonsView extends LinearLayout {
	private static final SparseArray<int[]> COLUMNS_BY_ROW = new SparseArray<int[]>();

	private ButtonsQuiz mQuiz;
	private int mAttemptsCounter = 0;
	private Button mStartButton;
	private QuizButtonsListener mQuizButtonsListener;
	private ArrayList<Button> mAnswerButtons;

	public interface QuizButtonsListener {
		public ButtonsQuiz onStart();
		public void onButtonClick(ButtonsQuiz quiz, ButtonQuizAnswer answer, int attemptsCounter);
	}

	public QuizButtonsView(Context context) {
		super(context, null);
	}

	public QuizButtonsView(Context context, AttributeSet attrs) {
		super(context, attrs);

		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.custom_quiz_buttons, this);	

		mStartButton = (Button) findViewById(R.id.btn_start);
		mStartButton.setOnClickListener(mStartButtonClickListener);
	}
	
	public void setButtonsListener(QuizButtonsListener listener) {
		mQuizButtonsListener = listener;
	}
	

	private OnClickListener mStartButtonClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			QuizButtonsView.this.removeView(mStartButton);

			if (mQuizButtonsListener != null) {
				QuizButtonsView.this.setupView(mQuizButtonsListener.onStart());
			}
		}
	};

	private OnClickListener mQuizButtonsClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			mAttemptsCounter++;

			int buttonIndex = ((Integer) v.getTag()).intValue();

			ButtonQuizAnswer answer = mQuiz.getAnswers()[buttonIndex];

			if (mQuizButtonsListener != null) {
				if (mAttemptsCounter == 1) {
					showResults(mQuiz.validateAnswer(answer));
				}
				
				mQuizButtonsListener.onButtonClick(mQuiz, answer, mAttemptsCounter);
			}
		}
	};

	public void setupView(ButtonsQuiz quiz) {
		// Reset variables
		mAttemptsCounter = 0;
		
		ButtonQuizAnswer[] answers = quiz.getAnswers();

		// Don't recreate buttons views if the new quiz has the same number of answers of the last quiz
		if (mQuiz == null || mQuiz.getAnswersCount() != quiz.getAnswersCount()) {
			LinearLayout optionsContainer = this;
			Context context = getContext();
			LayoutInflater inflater = LayoutInflater.from(context);

			int[] columnsByRow = COLUMNS_BY_ROW.get(answers.length, new int[]{});

			mAnswerButtons = new ArrayList<Button>();

			// Setup buttons view in a grid like way
			optionsContainer.setWeightSum(columnsByRow.length);
			optionsContainer.addView((View) inflater.inflate(R.layout.separator_horizontal, optionsContainer, false));
			for (int r = 0; r < columnsByRow.length; r++) {
				LinearLayout line = new LinearLayout(context);
				line.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, 1));

				for (int c = 0; c < columnsByRow[r]; c++) {
					Button b = (Button) inflater.inflate(R.layout.button_quiz_vertical, line, false);
					line.addView(b);

					// Add vertical separators only on non-last columns
					if (c < columnsByRow[r] - 1) {
						line.addView(inflater.inflate(R.layout.separator_vertical, line, false));
					}

					mAnswerButtons.add(b);
				}

				optionsContainer.addView(line);

				// Add horizontal separators only on non-last) rows
				if (r < columnsByRow.length - 1) {
					optionsContainer.addView((View) inflater.inflate(R.layout.separator_horizontal, optionsContainer, false));
				}	
			}
		}

		// Initialize buttons texts and stuff...
		for (int i = 0; i < mAnswerButtons.size(); i++) {
			Button button = mAnswerButtons.get(i);
			button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
			button.setText(answers[i].getLabel());
			button.setOnClickListener(mQuizButtonsClickListener);
			button.setTag(Integer.valueOf(i));
		}
		
		mQuiz = quiz;
		
		clearResults();
	}

	public void showResults(boolean onlyRightAnswer) {
		int rightColor = getResources().getColor(R.color.answer_right);
		int wrongColor = getResources().getColor(R.color.answer_wrong);

		ButtonQuizAnswer[] answers = mQuiz.getAnswers();
		for (int i = 0; i < answers.length; i++) {
			boolean isCorrect = mQuiz.validateAnswer(answers[i]);

			if (onlyRightAnswer) {
				if (isCorrect) {
					mAnswerButtons.get(i).setBackgroundColor(rightColor);
				}
			} else {
				mAnswerButtons.get(i).setBackgroundColor(isCorrect ? rightColor : wrongColor);
			}
		}
	}

	public void clearResults() {
		for (Button button : mAnswerButtons) {
			// FIXME: this won't work if I change to themes/styles (always white)
			button.setBackgroundColor(getResources().getColor(R.color.white));
		}
	}

	static {
		COLUMNS_BY_ROW.put(1, new int[]{1});
		COLUMNS_BY_ROW.put(2, new int[]{1, 1});
		COLUMNS_BY_ROW.put(3, new int[]{1, 1, 1});
		COLUMNS_BY_ROW.put(4, new int[]{2, 2});
		COLUMNS_BY_ROW.put(5, new int[]{2, 3});
		COLUMNS_BY_ROW.put(6, new int[]{3, 3});
		COLUMNS_BY_ROW.put(7, new int[]{1, 3, 3});
		COLUMNS_BY_ROW.put(8, new int[]{2, 3, 3});
		COLUMNS_BY_ROW.put(9, new int[]{3, 3, 3});
		COLUMNS_BY_ROW.put(10, new int[]{2, 2, 3, 3});
		COLUMNS_BY_ROW.put(11, new int[]{2, 3, 3, 3});
		COLUMNS_BY_ROW.put(12, new int[]{3, 3, 3, 3});
		COLUMNS_BY_ROW.put(13, new int[]{3, 3, 3, 4});
		COLUMNS_BY_ROW.put(14, new int[]{3, 3, 4, 4});
		COLUMNS_BY_ROW.put(15, new int[]{3, 4, 4, 4});
		COLUMNS_BY_ROW.put(16, new int[]{4, 4, 4, 4});
	}
}

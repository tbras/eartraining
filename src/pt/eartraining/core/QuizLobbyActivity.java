package pt.eartraining.core;

import java.util.ArrayList;
import java.util.HashMap;

import pt.eartraining.app.AppPreferences;
import pt.eartraining.app.R;
import pt.eartraining.core.SpecificationDialogFragment;
import pt.eartraining.core.QuizListFragment.QuizListFragmentInterface;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TabHost;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public abstract class QuizLobbyActivity extends SherlockFragmentActivity 
implements SpecificationDialogFragment.SpecificationDialogFragmentListener,
QuizListFragmentInterface {
	private static final String TAB_TAG_PREMADE = "premade";
	private static final String TAB_TAG_CUSTOM = "custom";

	private static final String TAG_DIALOG = "new_custom_quiz_dialog";

	protected static final int QUIZ_REQUEST_CODE = 0;

	private int mTempoInBPM;
	
	private ScoreDataSource mScoreManager;
	private TabHost mTabHost;
	private TabManager mTabManager;
	private SpecificationsDataSource mDataSource;
	private ArrayList<QuizSpecification> mPremades;
	private ArrayList<QuizSpecification> mCustoms;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_tabs);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		mTempoInBPM = AppPreferences.getInstance().getTempoInBPM();
		mScoreManager = new ScoreDataSource();
		mTabHost = (TabHost)findViewById(android.R.id.tabhost);
		mTabHost.setup();

		mTabManager = new TabManager(this, mTabHost, R.id.realtabcontent);
		mTabManager.addTab(mTabHost.newTabSpec(TAB_TAG_PREMADE).setIndicator(getString(R.string.tab_premade)),
				QuizListFragment.class, QuizListFragment.newFragmentArguments(TAB_TAG_PREMADE));
		mTabManager.addTab(mTabHost.newTabSpec(TAB_TAG_CUSTOM).setIndicator(getString(R.string.tab_custom)),
				QuizListFragment.class, QuizListFragment.newFragmentArguments(TAB_TAG_CUSTOM));

		mDataSource = getSpecsDataSource();

		mPremades = mDataSource.getSpecifications(DatabaseHelper.getDatabase(), false);
		mCustoms = mDataSource.getSpecifications(DatabaseHelper.getDatabase(), true);

		if (savedInstanceState != null) {
			mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("tab", mTabHost.getCurrentTabTag());
	}
	
	protected int getTempoInBPM() { return mTempoInBPM; }

	// ==================================================
	// QUIZ ACTIVITY REQUEST RESULT
	// ==================================================
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == QUIZ_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//            	Toast.makeText(this, "Update!", Toast.LENGTH_SHORT).show();
            	((QuizListFragment) mTabManager.getCurrentSeletedFragment()).refreshListView();
            } else {
//            	Toast.makeText(this, "Dont update!", Toast.LENGTH_SHORT).show();
            }
        }
	}

	// ==================================================
	// ACTION BAR MENU INTERFACE
	// ==================================================
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater().inflate(R.menu.custom_quiz, menu);

		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem item = menu.findItem(R.id.add);

		if (mTabManager.getCurrentSelectedTabId().equals(TAB_TAG_CUSTOM)) {
			item.setVisible(true);
		} else {
			item.setVisible(false);
		}

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch(item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
			// This button on appears in Custom View (user add quizzes)
		case R.id.add: 
			SpecificationDialogFragment dialog = getSpecDialogFragment();

			if (dialog != null) {
				dialog.show(getSupportFragmentManager(), TAG_DIALOG);
			}

			return true;
		default:return super.onMenuItemSelected(featureId, item);
		}
	}

	// ==================================================
	// HELPERS METHODS
	// ==================================================
	private ArrayList<QuizSpecification> getArrayListForTag(String tag) {
		if (tag.equals(TAB_TAG_PREMADE)) {
			return mPremades;
		} else {
			return mCustoms;
		}
	}
	
	protected QuizSpecification getQuizSpecification(String tag, int position) {
		return getArrayListForTag(tag).get(position);
	}

	// ==================================================
	// NEW CUSTOM SPECIFICATION INTERFACE
	// ==================================================
	@Override
	public void onDialogPositiveClick(QuizSpecification spec) {
		mCustoms.add(spec);

		QuizListFragment cFrag = (QuizListFragment)mTabManager.getCurrentSeletedFragment();

		cFrag.refreshListView();
	}

	// ==================================================
	// LIST FRAGMENT INTERFACE
	// ==================================================
	@Override
	public int getListItemsCount(String tag) {
		return getArrayListForTag(tag).size();
	}

	@Override
	public QuizListItem getListItem(String tag, int position) {
		QuizSpecification spec = getArrayListForTag(tag).get(position);

		return new QuizListItem((position + 1) + ". " + spec.getTitle(), 
				spec.getSubtitle(),
				mScoreManager.getHighScore(getSubject(), spec.getId()));
	}

	@Override
	public int getListItemId(String tag, int position) {
		return (int) getArrayListForTag(tag).get(position).getId();
	}

	@Override
	public void onListItemSelected(String tag, int position) {		
		QuizFactory factory = QuizFactory.newInstance(getQuizFactoryClass(), getQuizSpecification(tag, position));
		Intent intent = ButtonsQuizActivity.newIntent(this, factory);

		startActivityForResult(intent, QUIZ_REQUEST_CODE);
	}	

	// ==================================================
	// ABSTRACT METHODS
	// ==================================================
	protected abstract SpecificationDialogFragment getSpecDialogFragment();
	protected abstract SpecificationsDataSource getSpecsDataSource();
	protected abstract Class<?> getQuizFactoryClass();
	protected abstract EarTrainingSubject getSubject();

	/**
	 * Class taken from <b>ActionbarSherlock</b>.<br/><br/>
	 * 
	 * This is a helper class that implements a generic mechanism for
	 * associating fragments with the tabs in a tab host.  It relies on a
	 * trick.  Normally a tab host has a simple API for supplying a View or
	 * Intent that each tab will show.  This is not sufficient for switching
	 * between fragments.  So instead we make the content part of the tab host
	 * 0dp high (it is not shown) and the TabManager supplies its own dummy
	 * view to show as the tab content.  It listens to changes in tabs, and takes
	 * care of switch to the correct fragment shown in a separate content area
	 * whenever the selected tab changes.
	 */
	private static class TabManager implements TabHost.OnTabChangeListener {
		private final FragmentActivity mActivity;
		private final TabHost mTabHost;
		private final int mContainerId;
		private final HashMap<String, TabInfo> mTabs = new HashMap<String, TabInfo>();
		private TabInfo mLastTab;

		static final class TabInfo {
			private final String tag;
			private final Class<?> clss;
			private final Bundle args;
			private Fragment fragment;

			TabInfo(String _tag, Class<?> _class, Bundle _args) {
				tag = _tag;
				clss = _class;
				args = _args;
			}
		}

		static class DummyTabFactory implements TabHost.TabContentFactory {
			private final Context mContext;

			public DummyTabFactory(Context context) {
				mContext = context;
			}

			@Override
			public View createTabContent(String tag) {
				View v = new View(mContext);
				v.setMinimumWidth(0);
				v.setMinimumHeight(0);
				return v;
			}
		}

		public TabManager(FragmentActivity activity, TabHost tabHost, int containerId) {
			mActivity = activity;
			mTabHost = tabHost;
			mContainerId = containerId;
			mTabHost.setOnTabChangedListener(this);
		}

		public Fragment getCurrentSeletedFragment() {
			return mLastTab.fragment;
		}

		public String getCurrentSelectedTabId() {
			return mLastTab.tag;
		}

		public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
			tabSpec.setContent(new DummyTabFactory(mActivity));
			String tag = tabSpec.getTag();

			TabInfo info = new TabInfo(tag, clss, args);

			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state.  If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			info.fragment = mActivity.getSupportFragmentManager().findFragmentByTag(tag);
			if (info.fragment != null && !info.fragment.isDetached()) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
				ft.detach(info.fragment);
				ft.commit();
			}

			mTabs.put(tag, info);
			mTabHost.addTab(tabSpec);
		}

		@Override
		public void onTabChanged(String tabId) {
			TabInfo newTab = mTabs.get(tabId);
			if (mLastTab != newTab) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
				if (mLastTab != null) {
					if (mLastTab.fragment != null) {
						ft.detach(mLastTab.fragment);
					}
				}
				if (newTab != null) {
					if (newTab.fragment == null) {
						newTab.fragment = Fragment.instantiate(mActivity,
								newTab.clss.getName(), newTab.args);
						ft.add(mContainerId, newTab.fragment, newTab.tag);
					} else {
						ft.attach(newTab.fragment);
					}
				}

				mLastTab = newTab;
				ft.commit();
				mActivity.getSupportFragmentManager().executePendingTransactions();
			}

			// Refresh action bar
			mActivity.supportInvalidateOptionsMenu();
		}
	}
}

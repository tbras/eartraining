package pt.eartraining.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import pt.eartraining.app.App;

import android.database.sqlite.SQLiteDatabase;

public abstract class SpecificationsDataSource extends DataSource {
	protected static final String COLUMN_ID = "id";
	protected static final String COLUMN_SUBJECT = "subject";
	protected static final String COLUMN_CUSTOM = "custom";
	protected static final String COLUMN_TITLE = "title";
	protected static final String COLUMN_SUBTITLE = "subtitle";
	protected static final String COLUMN_VALIDATION_COUNT = "validation_count";

	public SpecificationsDataSource() {
//		SQLiteDatabase db = DatabaseHelper.getDatabase();
		
//		if (tableExists(db, dataSourceName()) == false) {
//			createTable(db, dataSourceName(), dataSourceColumns());
//			
//			loadPremadeSpecs(db);
//		}
	}

	// Public abstract methods		
	public abstract ArrayList<QuizSpecification> getSpecifications(SQLiteDatabase db, boolean custom);
	public abstract boolean addCustomSpec(SQLiteDatabase db, QuizSpecification spec);
	public abstract EarTrainingSubject getSubject();

	public static JSONArray loadJSONFile(int resId) {
		BufferedReader reader = null;
		StringBuilder sb = null;

		try {
			reader = new BufferedReader(new InputStreamReader(App.getContext().getResources().openRawResource(resId)));

			sb = new StringBuilder();
			String line;

			do {
				line = reader.readLine();

				if (line != null) {
					sb.append(line);
				}
			} while (line != null);

			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		JSONArray json = null;

		if (sb != null) {
			try {
				json = new JSONArray(sb.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		return json;
	}
}

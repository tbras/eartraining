package pt.eartraining.core;

import android.util.SparseArray;

public enum EarTrainingSubject {
	IntervalsComparison,
	IntervalsIdentification,
	ChordsIdentification,
	ChordsProgression,
	MelodicDictation;
	
	private static final SparseArray<EarTrainingSubject> intToSubjectMap = new SparseArray<EarTrainingSubject>();
	
	static {
	    for (EarTrainingSubject subject : EarTrainingSubject.values()) {
	        intToSubjectMap.put(subject.ordinal(), subject);
	    }
	}

	public static EarTrainingSubject fromInt(int i) {
		EarTrainingSubject subject = intToSubjectMap.get(Integer.valueOf(i));

	    return subject;
	}
}

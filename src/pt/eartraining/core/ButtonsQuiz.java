package pt.eartraining.core;

import pt.eartraining.soundengine.MusicTrack;

public class ButtonsQuiz {
	private MusicTrack mQuestion;
	private ButtonQuizAnswer[] mAnswers;
	private int mRightAnswerId;
	
	public ButtonsQuiz(MusicTrack question, ButtonQuizAnswer[] answers, int rightAnswerId) {
		mQuestion = question;
		mAnswers = answers;
		mRightAnswerId = rightAnswerId;
	}
	
	public MusicTrack getQuestion() { return mQuestion; }
	public boolean validateAnswer(ButtonQuizAnswer answer) { return answer.getId() == mRightAnswerId; }
	public ButtonQuizAnswer[] getAnswers() { return mAnswers; }
	public int getAnswersCount() { return mAnswers.length; }
	
	public static class ButtonQuizAnswer {
		private int mId;
		private String mLabel;
		private MusicTrack mTrack;
		
		/**
		 * 
		 * @param id This id should be unique for each subject, for example, intervals as id, 
		 * chords as id, ... For statistic purposes.
		 * @param label
		 * @param track
		 */
		public ButtonQuizAnswer(int id, String label, MusicTrack track) {
			mId = id;	// FIXME: maybe add an interface to clarify what kind of id should be used. (to retrieve values)
			mLabel = label;
			mTrack = track;
		}
		
		public int getId() {
			return mId;
		}

		public String getLabel() {
			return mLabel;
		}

		public MusicTrack getTrack() {
			return mTrack;
		}
	}
}

package pt.eartraining.core;

import pt.eartraining.core.DatabaseHelper.Column;
import pt.eartraining.core.DatabaseHelper.Column.ColumnType;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ScoreDataSource extends DataSource {
	public static final int EMPTY_SCORE = -1;
	
	private static final String COLUMN_ID = "id";
	private static final String COLUMN_SUBJECT = "subject";
	private static final String COLUMN_QUIZ_SPEC_ID = "quiz_spec_id";
	private static final String COLUMN_POINTS = "points";
	
	public ScoreDataSource() { }

	public int getHighScore(EarTrainingSubject subject, long quizSpecId) {
		String query = "SELECT " + COLUMN_POINTS + " FROM " + dataSourceName() +
				" WHERE " + COLUMN_SUBJECT + "=" + subject.ordinal() + " AND " + COLUMN_QUIZ_SPEC_ID + "=" + quizSpecId;
		Cursor cursor = DatabaseHelper.getDatabase().rawQuery(query, null);

		int points = EMPTY_SCORE;
		if (cursor != null) {
			if (cursor.moveToNext()) {
				points = cursor.getInt(0);
			}
			cursor.close();
		}

		return points;
	}

	public void updateHighScore(EarTrainingSubject subject, long quizSpecId, int points) {
		if (points < 0) { return; }

		int currentPoints = getHighScore(subject, quizSpecId);

		// Insert instead of update
		if (currentPoints == EMPTY_SCORE) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_SUBJECT, subject.ordinal());
			values.put(COLUMN_QUIZ_SPEC_ID, quizSpecId);
			values.put(COLUMN_POINTS, points);

			DatabaseHelper.getDatabase().insert(dataSourceName(), null, values);
		} else if (points > currentPoints) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_POINTS, points);

			String where = COLUMN_SUBJECT + "=" + subject.ordinal() + " AND " + COLUMN_QUIZ_SPEC_ID + "=" + quizSpecId;

			DatabaseHelper.getDatabase().update(dataSourceName(), values, where, null);
		}
	}

	@Override
	public String dataSourceName() {
		return "high_score";
	}

	@Override
	public Column[] dataSourceColumns() {
		return new Column[] {
			new Column(COLUMN_ID, ColumnType.INTEGER, true),
			new Column(COLUMN_SUBJECT, ColumnType.INTEGER),
			new Column(COLUMN_QUIZ_SPEC_ID, ColumnType.INTEGER),
			new Column(COLUMN_POINTS, ColumnType.INTEGER)
		};
	}

	@Override
	public void loadDefaultData(SQLiteDatabase db) {
		// There are no default values
	}
}

package pt.eartraining.core;

import android.os.Parcel;
import android.os.Parcelable;
import pt.eartraining.soundengine.MusicTrack;

public class KeyboardQuiz implements Parcelable {	
	private MusicTrack mQuestion;
	private int[] mAnswer; // List of notes

	public KeyboardQuiz(MusicTrack question, int[] answer) {
		mQuestion = question;
		mAnswer = answer.clone();
	}

	public MusicTrack getQuestion() { return mQuestion; }
	public int[] getAnswer() { return mAnswer; }

	public boolean validateAnswer(int[] answer, boolean[] rightNotes) { 
		assert(answer.length == rightNotes.length);

		boolean success = true;
		for (int i = 0; i < answer.length; i++) {
			rightNotes[i] = (mAnswer[i] == answer[i]); 

			if (rightNotes[i] == false) {
				success = false;
			}
		}

		return success;
	}

	// =======================================================
	// INTERFACE - Parcelable 
	// =======================================================
	protected KeyboardQuiz(Parcel in) {
		readFromParcel(in);
	}

	public void readFromParcel(Parcel in) {
		mQuestion = in.readParcelable((ClassLoader) MusicTrack.CREATOR);
		mAnswer = in.createIntArray();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(mQuestion, flags);
		dest.writeIntArray(mAnswer);
	}

	public static final Parcelable.Creator<KeyboardQuiz> CREATOR = new Parcelable.Creator<KeyboardQuiz>() {
		public KeyboardQuiz createFromParcel(Parcel in) {
			return new KeyboardQuiz(in);
		}

		public KeyboardQuiz[] newArray(int size) {
			return new KeyboardQuiz[size];
		}
	};
}

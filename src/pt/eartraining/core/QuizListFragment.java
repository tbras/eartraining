package pt.eartraining.core;

import pt.eartraining.app.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;

public class QuizListFragment extends SherlockListFragment {
	private static final String EXTRA_TAG = "tag";

	// Private fields
	private String mTag;
	private QuizSpecsAdapter mAdapter;
	private QuizListFragmentInterface mListInterface;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Bundle extras = getArguments();

		mTag = extras.getString(EXTRA_TAG);

		mAdapter = new QuizSpecsAdapter();

		setListAdapter(mAdapter);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mListInterface = (QuizListFragmentInterface) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + 
					" must implement QuizListFragmentInterface");
		}
	}
	
	public void refreshListView() {
		mAdapter.notifyDataSetChanged();
	}

	// =======================
	// LISTVIEW EVENTS
	// =======================
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		mListInterface.onListItemSelected(mTag, position);
	} 

	// =======================
	// STATIC HELPER
	// =======================
	public static Bundle newFragmentArguments(String tag) {
		Bundle extras = new Bundle();
		extras.putString(EXTRA_TAG, tag);
		return extras;
	}

	// =======================
	// LIST FRAGMENT INTERFACE
	// =======================
	public interface QuizListFragmentInterface {
		public int getListItemsCount(String tag);
		public QuizListItem getListItem(String tag, int position);
		public int getListItemId(String tag, int position);
		public void onListItemSelected(String tag, int position);
	}

	// =======================
	// ARRAY ADAPTER
	// =======================
	public class QuizSpecsAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			return mListInterface.getListItemsCount(mTag);
		}

		@Override
		public Object getItem(int position) {
			return mListInterface.getListItem(mTag, position);
		}

		@Override
		public long getItemId(int position) {
			return mListInterface.getListItemId(mTag, position);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(QuizListFragment.this.getSherlockActivity());
				convertView = inflater.inflate(R.layout.quiz_list_item, parent, false);
				
				holder = new ViewHolder();
				holder.title = (TextView)convertView.findViewById(R.id.row_title);
				holder.subtitle = (TextView)convertView.findViewById(R.id.row_subtitle);
				holder.score = (TextView)convertView.findViewById(R.id.row_score);
				
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			QuizListItem specs = (QuizListItem) getItem(position);

			String score = (specs.getHighScore() == ScoreDataSource.EMPTY_SCORE) ? 
					"--%" : String.valueOf(specs.getHighScore()) + "%";
			
			holder.title.setText(specs.getTitle());
			holder.subtitle.setText(specs.getSubtitle());
			holder.score.setText(score);

			return convertView;
		}
	}
	
	static class ViewHolder {
		public TextView title;
		public TextView subtitle;
		public TextView score;
	}
}

package pt.eartraining.core;

import java.util.ArrayList;

import pt.eartraining.app.App;
import pt.eartraining.chords.ChordsIdentificationDataSource;
import pt.eartraining.chords.ChordsProgressionsDataSource;
import pt.eartraining.dictation.MelodicDictationDataSource;
import pt.eartraining.intervals.IntervalsComparisonDataSource;
import pt.eartraining.intervals.IntervalsIdentificationLobby.IntervalsIdentificationDataSource;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper {
	public static String DATABASE_NAME = "eartraining.db";

	private static MySQLiteOpenHelper mSQLiteHelper = null;

	public static synchronized SQLiteDatabase getDatabase() {
		if (mSQLiteHelper == null) {
			ArrayList<DataSource> dataSources = new ArrayList<DataSource>();
			dataSources.add(new ScoreDataSource());
			dataSources.add(new IntervalsIdentificationDataSource());
			dataSources.add(new IntervalsComparisonDataSource());
			dataSources.add(new ChordsIdentificationDataSource());
			dataSources.add(new ChordsProgressionsDataSource());
			dataSources.add(new MelodicDictationDataSource());

			mSQLiteHelper = new MySQLiteOpenHelper(App.getContext(), dataSources);
		}

		return mSQLiteHelper.getWritableDatabase();
	}

	public static synchronized void clearAllData() {
		SQLiteDatabase db = getDatabase();
		
		mSQLiteHelper.clearData(db);
		mSQLiteHelper.vacuum(db);
		mSQLiteHelper.populateData(db);
	}

	private static class MySQLiteOpenHelper extends SQLiteOpenHelper {
		private static final int DATABASE_VERSION = 4;

		private ArrayList<DataSource> mDataSources;

		public MySQLiteOpenHelper(Context ctx, ArrayList<DataSource> dataSources) {
			super(ctx, DATABASE_NAME, null, DATABASE_VERSION);

			mDataSources = dataSources;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			createTables(db);
			populateData(db);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.i("Info", "Updating from v. " + oldVersion + " to v. " + newVersion +
					". All data will be reset.");
			
			dropTables(db);
			createTables(db);
			populateData(db);
		}

		public void createTables(SQLiteDatabase db) {
			for (DataSource dataSource : mDataSources) {
				createTable(db, dataSource.dataSourceName(), dataSource.dataSourceColumns());
			}
		}

		public void dropTables(SQLiteDatabase db) {
			for (DataSource dataSource : mDataSources) {
				dropTable(db, dataSource.dataSourceName());
			}
		}
		
		public void populateData(SQLiteDatabase db) {
			for (DataSource dataSource : mDataSources) {
				dataSource.loadDefaultData(db);
			}
		}

		public void clearData(SQLiteDatabase db) {
			for (DataSource dataSource : mDataSources) {
				db.delete(dataSource.dataSourceName(), null, null);
			}
		}
		
		public void vacuum(SQLiteDatabase db) {
			db.execSQL("VACUUM");
		}

		private boolean tableExists(SQLiteDatabase db, String tableName) {
			Cursor cursor = db.rawQuery("SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = '"+tableName+"'", null);

			if(cursor != null) {
				if(cursor.getCount() > 0) {
					cursor.close();
					return true;
				}

				cursor.close();
			}

			return false;
		}

		private void createTable(SQLiteDatabase db, String tableName, Column[] columns) {
			StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS " + tableName + " (");

			for (int i = 0; i < columns.length; i++) {
				sb.append(columns[i].toString());

				if (i == columns.length - 1) {
					sb.append(")");
				} else {
					sb.append(",");
				}
			}

			String createStmt = sb.toString();

			Log.i("DB Info", "Creating table: " + createStmt);

			db.execSQL(createStmt);
		}

		private void dropTable(SQLiteDatabase db, String tableName) {
			db.execSQL("DROP TABLE IF EXISTS " + tableName);
		}
	}

	public static class Column {
		public static enum ColumnType { NULL, INTEGER, REAL, TEXT };

		private String mName;
		private ColumnType mColumnType;
		private boolean mIsPK;

		public Column(String name, ColumnType columnType) {
			this(name, columnType, false);
		}

		public Column(String name, ColumnType columnType, boolean isPrimaryKey) { 
			mName = name; mColumnType = columnType; mIsPK = isPrimaryKey; 
		} 

		public String getName() { return mName; }
		public ColumnType getColumnType() { return mColumnType; }
		public boolean isPrimaryKey() { return mIsPK; }
		public String toString() { 
			return mName + " " + mColumnType.toString() + (mIsPK ? " PRIMARY KEY " : " ");		
		}
	}
}

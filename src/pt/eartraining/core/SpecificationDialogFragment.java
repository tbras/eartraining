package pt.eartraining.core;

import pt.eartraining.app.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;

public abstract class SpecificationDialogFragment extends SherlockDialogFragment {
	private View mContentView;
	private SpecificationDialogFragmentListener mListener;

	protected abstract View getDialogContentView();
	protected abstract QuizSpecification getSpecFromDialog(View dialogContentView);
	
	// ==================================================
	// Interface (Obligatory)
	// ==================================================
	public interface SpecificationDialogFragmentListener {
		public void onDialogPositiveClick(QuizSpecification spec);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		DialogInterface.OnClickListener positiveListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				mListener.onDialogPositiveClick(getSpecFromDialog(mContentView));
			}
		};

		DialogInterface.OnClickListener negativeListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Do nothing
			}
		};

		AlertDialog dialog = new AlertDialog.Builder(getSherlockActivity())
		.setTitle(R.string.alert_dialog_custom_interval_title)
		.setPositiveButton(R.string.alert_dialog_ok, positiveListener)
		.setNegativeButton(R.string.alert_dialog_cancel, negativeListener)
		.create();
		
		mContentView = getDialogContentView();

		if (mContentView == null) {
			TextView tv = new TextView(getSherlockActivity());
			tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			tv.setGravity(Gravity.CENTER);
			tv.setText("Not implemented!\nOverride this dialog!");
			tv.setPadding(20, 20, 20, 20);
			mContentView = tv;
		}
		
		dialog.setView(mContentView);

		return dialog;
	}	

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			mListener = (SpecificationDialogFragmentListener) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + 
					" must implement CustomSpecDialogListener");
		}
	}
}

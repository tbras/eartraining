package pt.eartraining.core;

public class QuizListItem {
	private String mTitle;
	private String mSubtitle;
	private int mHighScore;
	
	public QuizListItem(String title, String subtitle, int highScore) {
		mTitle = title;
		mSubtitle = subtitle;
		mHighScore = highScore;
	}

	public String getTitle() {
		return mTitle;
	}

	public String getSubtitle() {
		return mSubtitle;
	}

	public int getHighScore() {
		return mHighScore;
	}
}
